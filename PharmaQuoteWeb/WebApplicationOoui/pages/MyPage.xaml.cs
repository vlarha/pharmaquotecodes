﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using WebApplicationOoui.Contents;
using ServiceReference;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using WebApplicationOoui.Controllers;
using Ooui.Forms;

namespace WebApplicationOoui.pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MyPage : ContentPage
	{
        List<object> LO = new List<object>();
        VendorUser VU = new VendorUser();
        
        public MyPage ()
		{
			InitializeComponent ();
            var login = new Login(this);
            StkBase.Children.Add(login);
            Abs.RaiseChild(StkBase);
        }

        public async void SetMyContent(string content, List<object> X)
        {
            StkBase.Children.Clear();

            if (content == "Login")
            {                
                StkBase.Children.Add(new Login(this));
            }

            if (content == "Menu")
            {
                await FadeToMenu();
                Abs.RaiseChild(StkMenu);
                LO = X;
                VU = (VendorUser)X[0];
                LblUserName.Text = "Actual user: " + VU.UserName + "";
                LblVendor.Text = " " + VU.VendorName + " ";
            }

            if (content == "Main")
            {                
                VendorUser vu = (VendorUser)X[0];
                MainC MC = new MainC(this, vu);
                MC.UpdateList(0);
                StkBase.Children.Add(MC);
                Abs.RaiseChild(StkBase);
            }

            if (content == "CreateQuote")
            {
                VendorUser vu = (VendorUser)X[0];
                RFQs rf = (RFQs)X[1];
                int Type = (int)X[2];
                CreateQuote CQ = new CreateQuote(this,vu,rf,Type);
                CQ.UpdateList();
                StkBase.Children.Add(CQ);
            }

        }//End Set my content

        public async Task<string> DisplayAlerta(string Title, string Message, int i, string Accept, string Cancel)
        {
            string resp = "";
            if (i == 1)
            {
                var result = await DisplayAlert(Title, Message, Accept, Cancel);
                await DisplayAlert(Title, result.ToString(), Accept);
                
            }
            else
            {                
                await DisplayAlert(Title, Message, Accept);
            }
            return resp;
        }

        private async void BtnLogOut_Clicked(object sender, System.EventArgs e)
        {
            StkBase.Children.Clear();
            BtnBack.IsEnabled = false;
            StkBase.Children.Add(new Login(this));
            VU = new VendorUser();
            await FadeToBase();
            Abs.RaiseChild(StkBase);
        }


        //For create quotes
        private async void BtnQuotes_Clicked(object sender, System.EventArgs e)
        {
            await FadeToBase();
            BtnBack.IsEnabled = true;
            List<object> LO = new List<object>();
            LO.Add(VU);
            SetMyContent("Main", LO);
        }


        public async void ShowMenu()
        {
            await FadeToMenu();
            Abs.RaiseChild(StkMenu);            
        }

        private async void BtnBack_Clicked(object sender, System.EventArgs e)
        {
            await FadeToBase();
            Abs.RaiseChild(StkBase);
        }

        public async Task FadeToMenu()
        {
            uint length = 300;
            await Task.WhenAll(StkBase.FadeTo(0, length), StkMenu.FadeTo(1, length));                        
        }
        public async Task FadeToBase()
        {
            uint length = 300;
            await Task.WhenAll(StkBase.FadeTo(1, length), StkMenu.FadeTo(0, length));
        }
        
    }//End My Page
}