﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using WebApplicationOoui.Models;
using WebApplicationOoui.pages;
using Ooui.AspNetCore;
using Xamarin.Forms;
using System;

namespace WebApplicationOoui.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {            
            var page = new MyPage();            
            var element = page.GetOouiElement();
            return new ElementResult(element, "CSP Pharma Quote");            
        }

        public void OpenUrl()
        {
            Uri uri = new Uri("hhtp://vlarha.com:83");
            Device.OpenUri(uri);            
        }
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        
    }
}
