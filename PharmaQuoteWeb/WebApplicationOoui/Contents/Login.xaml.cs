﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ServiceReference;
using WebApplicationOoui.pages;
using System.Collections.Generic;

namespace WebApplicationOoui.Contents
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentView
    {
        public MyPage MP;
        public Login(MyPage mp)
        {
            InitializeComponent();
            this.HorizontalOptions = LayoutOptions.FillAndExpand;
            this.VerticalOptions = LayoutOptions.FillAndExpand;
            MP = mp;
        }

        //to close PupUp message
        private void BotonOk_Clicked(object sender, System.EventArgs e)
        {            
            LogInStack.IsVisible = true;
            PupUp.IsVisible = false;
        }//End BotonOk_Clicked

        //Click on button
        private async void Boton_Clicked(object sender, System.EventArgs e)
        {
            Boton.IsEnabled = false;
            ActIndi.Opacity = 1;
            ActIndi.IsRunning = true;
            try
            {
                ServiceClient SC = new ServiceClient();
                VendorUser vu = await SC.VendorUserByPassAsync(EntryUser.Text, EntryPass.Text);

                if (vu.Error)
                {
                    LblMsg.Text = vu.Msg;
                    LogInStack.IsVisible = false;
                    PupUp.IsVisible = true;
                    /*
                    EntryPass.IsEnabled = false;
                    EntryUser.IsEnabled = false;
                    Boton.IsEnabled = false;
                    string resp = await MP.DisplayAlerta("Message", vu.Msg, 0, "Ok", "Cancel");
                    EntryPass.IsEnabled = true;
                    EntryUser.IsEnabled = true;
                    Boton.IsEnabled = true;
                    */
                }
                else
                {
                    List<object> LO = new List<object>();
                    LO.Add(vu);
                    MP.SetMyContent("Menu",LO);
                }
            }
            catch (Exception ex)
            {                
                LblMsg.Text = ex.Message.ToString();
                LogInStack.IsVisible = false;
                PupUp.IsVisible = true;
                /*
                EntryPass.IsEnabled = false;
                EntryUser.IsEnabled = false;
                Boton.IsEnabled = false;
                string resp = await MP.DisplayAlerta("Message", ex.Message.ToString(), 0, "Ok", "Cancel");
                EntryPass.IsEnabled = true;
                EntryUser.IsEnabled = true;
                Boton.IsEnabled = true;
                */
            }


            try
            {
                ActIndi.IsRunning = false;
                ActIndi.Opacity = 0;
            }catch(Exception ee)
            {
                string j = ee.Message;
            }
            Boton.IsEnabled = true;            
        }//End Boton_Clicked

        private void EntryUser_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (EntryPass.Text != null && EntryUser.Text != null) { if (EntryPass.Text.Length > 0 && EntryUser.Text.Length > 0) Boton.IsEnabled = true; else Boton.IsEnabled = false; }
        }
        
    }
}