﻿using ServiceReference;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using WebApplicationOoui.pages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WebApplicationOoui.Contents
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CreateQuote : ContentView
	{

        public MyPage MP;
        public VendorUser VU;
        public RFQs RF;
        int Type = 0;//type = 1 is if sent

        public CreateQuote (MyPage mp, VendorUser vu, RFQs rf, int type)
		{
			InitializeComponent ();
            this.HorizontalOptions = LayoutOptions.FillAndExpand;
            this.VerticalOptions = LayoutOptions.FillAndExpand;
            MP = mp;
            VU = vu;
            RF = rf;
            Type = type;
        }

        public void UpdateList()
        {
            if (Type == 1)
            {
                BtnSend.Opacity = 0.1;
                BtnCancel.Text = "Back";                
                Swh.IsToggled = RF.Q.IncludedShipping;
                Swh.IsEnabled = false;
                Swh.IsVisible = false;
                LblOnly.Opacity = 1;
                LblSwh.IsVisible = true;
                if (RF.Q.IncludedShipping) LblSwh.Text = "Yes"; else LblSwh.Text = "No";
                if (!RF.Q.IncludedShipping) EntryCost.Text = RF.Q.ShippingCost.ToString("0.00");
                EntryCost.IsEnabled = false;
            }

            LblNumberOfRFQ.Text = RF.Solicitation;
            LblDue.Text = RF.Due.ToString("MM/dd/yyyy");
            LblItems.Text = "0" + " / " + RF.Details.Count.ToString();
            LblUserName.Text = "Actual user: " + VU.UserName;

            Center.Children.Clear();
            foreach (RFQsDetails RD in RF.Details)
            {
                bool CreateNew = true;
                foreach (QuoteBarView qb in Center.Children)
                {
                    if (RD.Id == qb.RdId())
                    {
                        CreateNew = false;
                        qb.Add_Sub(RD,qb,1);
                    }
                }//end foreach

                if (CreateNew)
                {
                    QuoteBarView QB = new QuoteBarView(RD, this, Type);
                    QB.FillFields();
                    Center.Children.Add(QB);
                }
            }
            Val();
        }

        private void BtnLogOut_Clicked(object sender, System.EventArgs e)
        {
            MP.SetMyContent("Login", new  List<object>());
        }


        public void Val()
        {
            int er = 0;
            int Cant = 0;
            BtnSend.IsEnabled = false;
            foreach (QuoteBarView q in Center.Children)
            {
                er = er + q.ValOut();
                Cant = Cant + q.Cant;
            }
            if (er == 0 && ValCost() == 0 && Type==0 && Cant > 0) BtnSend.IsEnabled = true; else BtnSend.IsEnabled = false;
            LblItems.Text = (RF.Details.Count - er) + " / " + RF.Details.Count.ToString();
            
        }

        private void Swh_Toggled(object sender, ToggledEventArgs e)
        {
            if (Swh.IsToggled)
            {
                LblCost.IsEnabled = false;
                LblCost.Opacity = 0.3;
                EntryCost.IsEnabled = false;
                EntryCost.Opacity = 0.3;
            }else
            {
                LblCost.IsEnabled = true;
                LblCost.Opacity = 1;
                EntryCost.IsEnabled = true;
                EntryCost.Opacity = 1;
            }
            Val();
        }

        private void BtnCancel_Clicked(object sender, System.EventArgs e)
        {
            List<object> LO = new List<object>();
            LO.Add(VU);
            MP.SetMyContent("Main", LO);
        }

        private void EntryCost_TextChanged(object sender, TextChangedEventArgs e)
        {
            Val();
        }

        private int ValCost()
        {
            Regex Re;
            int er = 0;
            EntryCost.BackgroundColor = Color.LightSalmon;
            Re = new Regex(@"^[0-9]*(?:\.[0-9]*)?$");
            if ((Re.IsMatch(EntryCost.Text) && EntryCost.Text.ToString().Length > 0) || Swh.IsToggled) EntryCost.BackgroundColor = Color.White; else er = 1;
            return er;
        }

        private async void BtnSend_Clicked(object sender, System.EventArgs e)
        {
            BtnSend.IsEnabled = false;
            LblStatus.Text = "Status: ";

            Quotes Q = new Quotes();
            Q.Details = new List<QuotesDetails>();

            Q.IdVendor = VU.IdVendor;
            Q.IdRFQs = RF.Id;
            Q.Status = true;
            Q.IncludedShipping = Swh.IsToggled;
            Q.ShippingCost = Swh.IsToggled ? 0 : float.Parse(EntryCost.Text);
            //Q.Created is SetBinding on service with the data base date Getdate()
            foreach (QuoteBarView Qb in Center.Children)
            {
                foreach (QuotesDetails qd in Qb.QD())
                {
                    Q.Details.Add(qd);
                }
            }

            try
            {
                ServiceClient Sc = new ServiceClient();
                string resp = await Sc.SaveQuotePerVendorAsync(Q);
                if (resp == "Ok")
                {
                    List<object> LO = new List<object>();
                    LO.Add(VU);
                    MP.SetMyContent("Main", LO);
                }
            }
            catch (Exception ex)
            {
                LblStatus.Text = "Status: " + "Communication error. Try again.";
            }           
            BtnSend.IsEnabled = true;
        }//BtnSend_Clicked

        //Show the menu
        private void BtnMenu_Clicked(object sender, EventArgs e)
        {
            MP.ShowMenu();
        }

    }//End Class
}