﻿using System;
using System.IO;
using System.Net.Http;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WebApplicationOoui.Contents
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Welcome : ContentView
	{
		public Welcome ()
		{
			InitializeComponent ();
		}        
    }
}