﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ServiceReference;
using WebApplicationOoui.pages;
using System.Collections.Generic;
using System;
using System.Timers;
using System.Threading.Tasks;

namespace WebApplicationOoui.Contents
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainC : ContentView
	{
        VendorUser VU = new VendorUser();
        public MyPage MP;

		public MainC (MyPage mp, VendorUser vu )
		{
			InitializeComponent ();
            this.HorizontalOptions = LayoutOptions.FillAndExpand;
            this.VerticalOptions = LayoutOptions.FillAndExpand;
            VU = vu;
            MP = mp;            
        }

        public async void UpdateList( int i)
        {

            BtnRefresh.IsEnabled = false;
            Center.Children.Clear();
            Center2.Children.Clear();
            LblNumberOfRFQ.Text = "0";
            LblSent.Text = "0";
            LblUserName.Text = "Actual user: " + VU.UserName;


            try
            {
                ServiceClient SC = new ServiceClient();
                RFQsList RL = await SC.RFQListPerVendorAsync(VU.IdVendor);


                LblNumberOfRFQ.Text = RL.Lista.Count.ToString();

                foreach (RFQs r in RL.Lista)
                {
                    RFQBarView RFQBV = new RFQBarView(MP, VU, this, r, 0);
                    RFQBV.Show();
                    Center.Children.Add(RFQBV);
                }
                await SC.CloseAsync();

                //Sent RFQs
                ServiceClient SC2 = new ServiceClient();
                RL = await SC2.RFQListSentPerVendorAsync(VU.IdVendor);
                LblSent.Text = RL.Lista.Count.ToString();
                foreach (RFQs r in RL.Lista)
                {                    
                    RFQBarView RFQBV = new RFQBarView(MP, VU, this, r, 1);
                    RFQBV.Show();
                    Center2.Children.Add(RFQBV);
                }
                await SC2.CloseAsync();
            }
            catch (Exception ex)
            {
                string str = ex.Message;
                if (i < 5) UpdateList(i + 1);
            }
            BtnRefresh.IsEnabled = true;
        }//End Update List        

        private void BtnLogOut_Clicked(object sender, System.EventArgs e)
        {
            MP.SetMyContent("Login", new List<object>());
        }

        private async void BtnRefresh_Clicked(object sender, EventArgs e)
        {
            //await AnimateWinAsync(true);
            //MP.DisplayAlerta();
            UpdateList(0);
        }

        private void BtnMenu_Clicked(object sender, EventArgs e)
        {
            MP.ShowMenu();
        }
    }
}