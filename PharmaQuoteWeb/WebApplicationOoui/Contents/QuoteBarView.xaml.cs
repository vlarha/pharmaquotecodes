﻿using ServiceReference;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;

namespace WebApplicationOoui.Contents
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class QuoteBarView : ContentView
	{
        RFQsDetails RD;
        CreateQuote CQ;
        int Type = 0; //type = 1 is if sent
        public int Cant = 0;


        //Return the Rd Id
        public long RdId()
        {
            return RD.Id;
        }//End RdId
        
        public QuoteBarView (RFQsDetails rd, CreateQuote cq, int type)
		{
			InitializeComponent ();
            RD = rd;
            CQ = cq;
            Type = type;
            this.Opacity = 0;
            if (Type == 1) BtnAdd.IsEnabled = false;
        }

        public async void FillFields()
        {
            LblDescription.Text = RD.Description;
            LblNDC.Text = RD.NDC;
            LblQty.Text = RD.Qty.ToString();
            //LblWeCanNot.Text = "I am unable to quote a this NDC: " + RD.NDC + " at this time.";
            //LblQtyToBe.Text = "(" + RD.Qty.ToString() + ")";
            if (Type == 1)
            {
                //BtnSwh.IsEnabled = false;
                //EntryAltNDC.IsEnabled = false;
                //EntryAltQty.IsEnabled = false;
                //EntryComments.IsEnabled = false;
                //EntryCountry.IsEnabled = false;
                //EntryCSP.IsEnabled = false;
                //EntryManufacturer.IsEnabled = false;
                //EntrySP.IsEnabled = false;
                //EntryTotal.IsEnabled = false;
                Add_Sub(RD, this, Type);
                if (RD.QD.Unable)
                {
                    BtnSwh_Clicked(new object(), new System.EventArgs());
                }
                else
                {
                    //EntryAltNDC.Text = RD.QD.AltNDC;
                    //EntryAltQty.Text = RD.QD.AltQty.ToString("0.00");
                    //EntryComments.Text = RD.QD.Comments;
                    //EntryCountry.Text = RD.QD.CountryOfOrigin;
                    //EntryCSP.Text = RD.QD.CSPPrice.ToString("0.00");
                    //EntryManufacturer.Text = RD.QD.Manufacturer;
                    //EntrySP.Text = RD.QD.SugestedUnitPrice.ToString("0.00");
                    //EntryTotal.Text = RD.QD.Total.ToString("0.00");                    
                }
            }
            else
            {
                BtnAdd_Clicked(new object(), new System.EventArgs());
            }
            await AnimateWinAsync(true);
        }//End FillFields

        public List<QuotesDetails> QD()
        {
            List<QuotesDetails> ListQd = new List<QuotesDetails>();
            foreach (SubQuoteBarView SQ in StkCoreQuoteVertical.Children)
            {
                ListQd.Add(SQ.QD());
            }
            /*
            if (BtnSwh.Text == "O")//Not Checked
            {
                qd.IdRFQDetail = RD.Id;
                qd.AltNDC = "";
                qd.AltQty = 0;
                
                qd.SugestedUnitPrice = float.Parse(EntrySP.Text);
                qd.CSPPrice = float.Parse(EntryCSP.Text);
                qd.Total = float.Parse(EntryTotal.Text);
                qd.Manufacturer = EntryManufacturer.Text;
                qd.CountryOfOrigin = EntryCountry.Text;
                qd.Comments = EntryComments.Text;
                qd.Unable = false;
                

            }//End if
            else//Unchecked
            {
                qd.IdRFQDetail = RD.Id;
                qd.AltNDC = "";
                qd.AltQty = 0;
                qd.SugestedUnitPrice = 0;
                qd.CSPPrice = 0;
                qd.Total = 0;
                qd.Manufacturer = "";
                qd.CountryOfOrigin = "";
                qd.Comments = "";
                qd.Unable = true;
            }//End Else
            */
            return ListQd;
        }//End QD

        private void EntryText_TextChanged(object sender, TextChangedEventArgs e)
        {
            Entry en = (Entry)sender;           
            int i = Val();
        }


        public int Val()
        {
            //Regex Re;
            
            int er = 0;
            /*
            Bck.BackgroundColor = Color.LightSalmon;

            foreach (object SQ in StkCoreQuoteVertical.Children)
            {
                try
                {
                    SubQuoteBarView s = (SubQuoteBarView)SQ;
                    er = er + s.ValOut();
                }
                catch (Exception e)
                {
                }
            }
            */
            /*
            //Sugested Price
            EntrySP.BackgroundColor = Color.LightSalmon;
            Re = new Regex(@"^[0-9]*(?:\.[0-9]*)?$");
            if (Re.IsMatch(EntrySP.Text) && EntrySP.Text.ToString().Length > 0) EntrySP.BackgroundColor = Color.White; else er = 1;

            //CSP Price
            EntryCSP.BackgroundColor = Color.LightSalmon;
            Re = new Regex(@"^[0-9]*(?:\.[0-9]*)?$");
            if (Re.IsMatch(EntryCSP.Text) && EntryCSP.Text.ToString().Length > 0) EntryCSP.BackgroundColor = Color.White; else er = 1;

            //Total Price
            EntryTotal.BackgroundColor = Color.LightSalmon;
            Re = new Regex(@"^[0-9]*(?:\.[0-9]*)?$");
            if (Re.IsMatch(EntryTotal.Text) && EntryTotal.Text.ToString().Length > 0) EntryTotal.BackgroundColor = Color.White; else er = 1;

            //Manufacturer
            EntryManufacturer.BackgroundColor = Color.LightSalmon;            
            if (EntryManufacturer.Text.ToString().Length > 2) EntryManufacturer.BackgroundColor = Color.White; else er = 1;

            //Country
            EntryCountry.BackgroundColor = Color.LightSalmon;
            if (EntryCountry.Text.ToString().Length > 1) EntryCountry.BackgroundColor = Color.White; else er = 1;

            //Alt Qty
            EntryAltQty.BackgroundColor = Color.LightSalmon;
            Re = new Regex(@"^[0-9]*(?:\.[0-9]*)?$");
            if (Re.IsMatch(EntryAltQty.Text) && EntryAltQty.Text.ToString().Length > 0) EntryAltQty.BackgroundColor = Color.White; else er = 1;

            if (BtnSwh.Text == "X") er = 0;
            */
            if (er == 0) Bck.BackgroundColor = Color.LightGreen;

            CQ.Val();
            return er;
            
        }//End Val


        public int ValOut()
        {
            //Regex Re;
            int er = 0;
            Cant = 0;

            foreach (object SQ in StkCoreQuoteVertical.Children)
            {
                Cant = Cant + 1;
                try
                {
                    SubQuoteBarView s = (SubQuoteBarView)SQ;
                    er = er + s.ValOut();
                    
                }
                catch (Exception e)
                {
                }
            }

            Bck.BackgroundColor = Color.LightSalmon;
            /*
            //Sugested Price
            EntrySP.BackgroundColor = Color.LightSalmon;
            Re = new Regex(@"^[0-9]*(?:\.[0-9]*)?$");
            if (Re.IsMatch(EntrySP.Text) && EntrySP.Text.ToString().Length > 0) EntrySP.BackgroundColor = Color.White; else er = 1;

            //CSP Price
            EntryCSP.BackgroundColor = Color.LightSalmon;
            Re = new Regex(@"^[0-9]*(?:\.[0-9]*)?$");
            if (Re.IsMatch(EntryCSP.Text) && EntryCSP.Text.ToString().Length > 0) EntryCSP.BackgroundColor = Color.White; else er = 1;

            //Total Price
            EntryTotal.BackgroundColor = Color.LightSalmon;
            Re = new Regex(@"^[0-9]*(?:\.[0-9]*)?$");
            if (Re.IsMatch(EntryTotal.Text) && EntryTotal.Text.ToString().Length > 0) EntryTotal.BackgroundColor = Color.White; else er = 1;

            //Manufacturer
            EntryManufacturer.BackgroundColor = Color.LightSalmon;
            if (EntryManufacturer.Text.ToString().Length > 2) EntryManufacturer.BackgroundColor = Color.White; else er = 1;

            //Country
            EntryCountry.BackgroundColor = Color.LightSalmon;
            if (EntryCountry.Text.ToString().Length > 1) EntryCountry.BackgroundColor = Color.White; else er = 1;

            //Alt Qty
            EntryAltQty.BackgroundColor = Color.LightSalmon;
            Re = new Regex(@"^[0-9]*(?:\.[0-9]*)?$");
            if (Re.IsMatch(EntryAltQty.Text) && EntryAltQty.Text.ToString().Length > 0) EntryAltQty.BackgroundColor = Color.White; else er = 1;

            if (BtnSwh.Text == "X") er = 0;
            */
            if (er == 0) Bck.BackgroundColor = Color.LightGreen;

            return er;
        }//End Val

        private void BtnSwh_Clicked(object sender, System.EventArgs e)
        {
            /*
            if (BtnSwh.Text=="X")
            {
                StkCoreQuote.IsEnabled = true;
                StkCoreQuote.Opacity = 1;
                //Bck.BackgroundColor = Color.LightSalmon;
                BtnSwh.Text = "O";
                BtnSwh.BackgroundColor = Color.LightBlue;

                foreach (object obj in StkCoreQuote.Children)
                {
                    if (obj.GetType().ToString() == "Xamarin.Forms.StackLayout")
                    {
                        StackLayout stk = (StackLayout)obj;
                        stk.Opacity = 1;
                        EntryAltNDC.IsEnabled = true;
                        EntryAltQty.IsEnabled = true;
                        EntrySP.IsEnabled = true;
                        EntryCSP.IsEnabled = true;
                        EntryTotal.IsEnabled = true;
                        EntryManufacturer.IsEnabled = true;
                        EntryCountry.IsEnabled = true;
                        EntryComments.IsEnabled = true;
                    }
                }//End for each
            }//End if
            else
            {
                StkCoreQuote.IsEnabled = false;
                StkCoreQuote.Opacity = 0.4;
                //Bck.BackgroundColor = Color.LightGreen;
                BtnSwh.Text = "X";                
                BtnSwh.BackgroundColor = Color.LightCoral;

                foreach (object obj in StkCoreQuote.Children)
                {
                    if (obj.GetType().ToString() == "Xamarin.Forms.StackLayout")
                    {
                        StackLayout stk = (StackLayout)obj;
                        stk.Opacity = 0.4;
                        EntryAltNDC.IsEnabled = false;
                        EntryAltQty.IsEnabled = false;
                        EntrySP.IsEnabled = false;
                        EntryCSP.IsEnabled = false;
                        EntryTotal.IsEnabled = false;
                        EntryManufacturer.IsEnabled = false;
                        EntryCountry.IsEnabled = false;
                        EntryComments.IsEnabled = false;
                    }
                }//End for each
            }//End Else
            int i = Val();
            */
        }//End Swh Toggled 

        public async Task AnimateWinAsync(bool isReverse)
        {
            uint length = 750;
            await Task.WhenAll(this.FadeTo(0, length));
            await Task.WhenAll(this.FadeTo(1, length));
            this.Opacity = 1;
        }

        private void BtnAdd_Clicked(object sender, System.EventArgs e)
        {
            BtnAdd.IsEnabled = false;
            SubQuoteBarView SQB = new SubQuoteBarView(this,RD);
            //SQB.Opacity = 0;
            StkCoreQuoteVertical.Children.Add(SQB);                       
            LblNumber.Text = (1 + Convert.ToInt32(LblNumber.Text)).ToString();
            
            //await Task.WhenAll(SQB.FadeTo(1, 750));
            SQB.Opacity = 1;
            Val();
            BtnAdd.IsEnabled = true;
        }


        public void Add_Sub(RFQsDetails rd, QuoteBarView qb, int type)
        {
            
            SubQuoteBarView SQB = new SubQuoteBarView(this, RD);
            //SQB.Opacity = 0;
            StkCoreQuoteVertical.Children.Add(SQB);
            LblNumber.Text = (1 + Convert.ToInt32(LblNumber.Text)).ToString();
            SQB.FillFields(rd, qb, type);
            //await Task.WhenAll(SQB.FadeTo(1, 750));
            SQB.Opacity = 1;
            Val();
            
        }//End Add Sub

        public void RemoveChild(SubQuoteBarView SQbv)
        {            
            LblNumber.Text = ( Convert.ToInt32(LblNumber.Text)-1).ToString();
            StkCoreQuoteVertical.Children.Remove(SQbv);
            Val();           
        }
        
    }
}