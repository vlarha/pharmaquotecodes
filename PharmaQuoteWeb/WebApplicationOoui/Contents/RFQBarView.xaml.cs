﻿using ServiceReference;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplicationOoui.pages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WebApplicationOoui.Contents
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RFQBarView : ContentView
	{
        public MyPage MP;
        VendorUser VU = new VendorUser();
        MainC MC = new MainC(new MyPage(),new VendorUser());
        RFQs RF = new RFQs();
        int Type = 0;  //type = 1 is if sent    

        public RFQBarView (MyPage mp, VendorUser vu, MainC mc, RFQs rf, int type)    //type = 1 is sent    
        {
			InitializeComponent ();
            MP = mp;
            VU = vu;
            MC = mc;
            RF = rf;
            Type = type;
            this.Opacity = 0;
		}
        public async void Show()
        {
            LblRFQN.Text = RF.Solicitation;
            LblDueDate.Text = RF.Due.ToString("MM/dd/yyyy");            
            LblItems.Text = RF.Details.Count.ToString();
            if (Type == 1)
            {
                LblCre.Text = "Sent: ";
                LblCreated.Text = RF.Sent.ToString("MM/dd/yyyy");
            }
            else
            {
                LblCreated.Text = RF.Created.ToString("MM/dd/yyyy");
            }
            await AnimateWinAsync(true);
        }

        private void BtnView_Clicked(object sender, System.EventArgs e)
        {
            List<object> LO = new List<object>();
            LO.Add(VU);
            LO.Add(RF);
            LO.Add(Type);
            MP.SetMyContent("CreateQuote", LO);
        }

        public async Task AnimateWinAsync(bool isReverse)
        {
            uint length = 750;
            await Task.WhenAll(this.FadeTo(0, length));            
            await Task.WhenAll(this.FadeTo(1, length));
            this.Opacity = 1;
            //await Task.WhenAll(this.ScaleTo(3, 0), this.FadeTo(0, 0));
            //await Task.WhenAll(this.ScaleTo(1, length / 2), this.FadeTo(1, length));
            //this.Rotation = 0;
        }


    }//End Class RFQBarView
}