﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using PharmaQuoteService.Classes;

namespace PharmaQuoteService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService" in both code and config file together.
    [ServiceContract]
    public interface IService
    {
        [OperationContract]
        QuotesList QuotesListPerRFQ(long IdRFQ);

        [OperationContract]
        QuotesPerRFQsList QtyQuotesPerRFQs(QuotesPerRFQsParams Params);

        [OperationContract]
        string SaveQuotePerVendor(Quotes q);

        [OperationContract]
        TemplatesList Templates_List();

        [OperationContract]
        string AddTemplate(Templates t);

        [OperationContract]
        long CheckRFQ(string SN);

        [OperationContract]
        string SaveRFQs(RFQs Rf);

        [OperationContract]        
        string SendEmails(string email, string name, string SN);

        [OperationContract]
        VendorUser VendorUserByPass(string UserName, string Password);

        [OperationContract]
        RFQsList RFQListPerVendor(long IdVendor);

        [OperationContract]
        RFQsList RFQListSentPerVendor(long IdVendor);

        [OperationContract]
        RFQsList RFQPerId(long Id);

        [OperationContract]
        string Pdf();

    }

}
