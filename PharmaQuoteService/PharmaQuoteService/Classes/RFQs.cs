﻿using System;
using System.Collections.Generic;


namespace PharmaQuoteService.Classes
{
    public class RFQs
    {
        public long Id
        { get; set; }
        public long IdUser
        { get; set; }
        public string Template
        { get; set; }
        public string Solicitation
        { get; set; }
        public DateTime Posted
        { get; set; }
        public DateTime Due
        { get; set; }
        public string Customer
        { get; set; }
        public string Status
        { get; set; }
        public string FileName
        { get; set; }
        public DateTime Created
        { get; set; }
        public int Items
        { get; set; }
        public DateTime Sent
        { get; set; }
        public Quotes Q
        { get; set; }
        public bool Error
        { get; set; }
        public string Msg
        { get; set; }
        public List<RFQsDetails> Details
        { get; set; }        

        public RFQs()
        {
            Id = 0;
            IdUser = 0;
            Template = "";
            Solicitation = "";
            Posted = DateTime.Now;
            Due = DateTime.Now;
            Customer = "";
            Status = "";
            FileName = "";
            Created = DateTime.Now;
            Items = 0;
            Sent = DateTime.Now;
            Q = new Quotes();
            Details = new List<RFQsDetails>();
            Error = false;
            Msg = "";            
        }//End Constructor

        public RFQs(long id, long Ids, string t, string s, DateTime P, DateTime S, string C, string st, string fi, DateTime Cr, bool er, string ms, int It, DateTime se, Quotes q)
        {
            Id = 0;
            IdUser = 0;
            Template = t;
            Solicitation = s;
            Posted = P;
            Due = S;
            Customer = C;
            Status = st;
            FileName = fi;
            Created = Cr;
            Items = It;
            Sent = se;
            Details = new List<RFQsDetails>();
            Q = q;
            Error = er;
            Msg = ms;
        }//End Constructor                 
    }//End RFQs

    //RFQsList///////////////////////////
    public class RFQsList
    {
        public List<RFQs> Lista
        { get; set; }
        public bool Error
        { get; set; }
        public string Msg
        { get; set; }

        public RFQsList()
        {
            Lista = new List<RFQs>();
            Error = false;
            Msg = "";
        }
    }//End RFQs List


    //RFQsDetails///////////////////////////
    public class RFQsDetails
    {
        public long Id
        { get; set; }
        public long IdRFQ
        { get; set; }
        public string NDC
        { get; set; }
        public float Qty
        { get; set; }
        public string Description
        { get; set; }
        public bool Error
        { get; set; }
        public string Msg
        { get; set; }
        public QuotesDetails QD
        { get; set; }

        public RFQsDetails()
        {
            Id = 0;
            IdRFQ = 0;
            NDC = "";
            Qty = 0;
            Description = "";
            QD = new QuotesDetails();
            Error = false;
            Msg = "";
        }//End Constructor

     
        public RFQsDetails(long id, long Idr, string N, float Q, string D, bool er, string ms, QuotesDetails qd)
        {
            Id = id;
            IdRFQ = Idr;
            NDC = N;
            Qty = Q;
            Description = D;
            QD = qd;
            Error = false;
            Msg = "";
        }//End Constructor        
    }//End RFQsDetails   
}//End NameSPace