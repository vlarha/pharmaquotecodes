﻿using System.Collections.Generic;

namespace PharmaQuoteService.Classes
{
    public class VendorUser
    {
        public long Id
        { get; set; }
        public long IdVendor
        { get; set; }
        public string UserName
        { get; set; }
        public string Password
        { get; set; }
        public string Email
        { get; set; }
        public string Phone
        { get; set; }
        public long IdType
        { get; set; }
        public bool Status
        { get; set; }
        public string FirstName
        { get; set; }
        public string LastName
        { get; set; }
        public bool Receiver
        { get; set; }
        public string VendorName
        { get; set; }
        public bool Error
        { get; set; }
        public string Msg
        { get; set; }

        public VendorUser()
        {
            Id = 0;
            IdVendor = 0;
            UserName = "";
            Password = "";
            Email = "";
            Phone = "";
            IdType = 0;
            Status = true;
            FirstName = "";
            LastName = "";
            VendorName = "";
            Receiver = true;
            Error = false;
            Msg = "";
        }//End Constructor

        public VendorUser(long id, long IdV, string Un, string Pa, string Em, string Ph, long Type, bool stat, string fn, string ln, bool Re, string vn)
        {
            Id = id;
            IdVendor = IdV;
            UserName = Un;
            Password = Pa;
            Email = Em;
            Phone = Ph;
            IdType = Type;
            Status = stat;
            FirstName = fn;
            LastName = ln;
            Receiver = Re;
            VendorName = vn;
        }//End Constructor
    }//End Vendor User

    public class VendorUserList
    {
        public List<VendorUser> List
        { get; set; }
        public bool Error
        { get; set; }
        public string Msg
        { get; set; }

        public VendorUserList()
        {
            List = new List<VendorUser>();
            Error = false;
            Msg = "";
        }
    }//End Templates List 
}