﻿using System.Collections.Generic;

namespace PharmaQuoteService.Classes
{    
    public class Templates
    {
        public long Id
        { get; set; }
        public string Name
        { get; set; }
        public string Template
        { get; set; }
        public bool Error
        { get; set; }
        public string Msg
        { get; set; }


        public Templates()
        {
            Id = 0;
            Name = "";
            Template = "";
            Error = false;
            Msg = "";
        }//End Constructor

        public Templates(long id, string n, string t, bool er, string ms)
        {
            Id = id;
            Name = n;
            Template = t;
            Error = er;
            Msg = ms;
        }//End Constructor

    }//End Templates

    public class TemplatesList
    {
        public List<Templates> List
        { get; set; }
        public bool Error
        { get; set; }
        public string Msg
        { get; set; }

        public TemplatesList()
        {
            List = new List<Templates>();
            Error = false;
            Msg = "";
        }
    }//End Templates List    
}