﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PharmaQuoteService.Classes
{
    public class Quotes
    {
        public long Id
        { get; set; }
        public long IdVendor
        { get; set; }
        public long IdRFQs
        { get; set; }
        public DateTime Created
        { get; set; }
        public bool Status
        { get; set; }
        public bool IncludedShipping
        { get; set; }
        public double ShippingCost
        { get; set; }
        public string VendorName
        { get; set; }
        public bool Error
        { get; set; }
        public string Msg
        { get; set; }
        public List<QuotesDetails> Details
        { get; set; }

        public Quotes()
        {
            Id = 0;
            IdVendor = 0;
            IdRFQs = 0;
            Created = DateTime.Now;
            Status = true;
            IncludedShipping = false;
            ShippingCost = 0;
            Details = new List<QuotesDetails>();
            VendorName = "";
            Error = false;
            Msg = "";
        }//End Constructor
    }//End Quotes

    //QuotesList///////////////////////////
    public class QuotesList
    {
        public List<Quotes> Lista
        { get; set; }
        public bool Error
        { get; set; }
        public string Msg
        { get; set; }

        public QuotesList()
        {
            Lista = new List<Quotes>();
            Error = false;
            Msg = "";
        }
    }//End Quotes List


    //QuotesDetails///////////////////////////
    public class QuotesDetails
    {
        public long Id
        { get; set; }
        public long IdVendorQuotes
        { get; set; }
        public long IdRFQDetail
        { get; set; }
        public float SugestedUnitPrice
        { get; set; }
        public float CSPPrice
        { get; set; }
        public string Comments
        { get; set; }
        public float AltQty
        { get; set; }
        public float Total
        { get; set; }
        public string AltNDC
        { get; set; }
        public string Manufacturer
        { get; set; }
        public string CountryOfOrigin
        { get; set; }
        public bool Unable
        { get; set; }
        public bool Error
        { get; set; }
        public string Msg
        { get; set; }

        public QuotesDetails()
        {
            Id = 0;
            IdVendorQuotes = 0;
            IdRFQDetail = 0;
            SugestedUnitPrice = 0;
            CSPPrice = 0;
            Comments = "";
            AltQty = 0;
            AltNDC = "";
            Total = 0;
            Manufacturer = "";
            CountryOfOrigin = "";
            Unable = false;
            Error = false;
            Msg = "";
        }//End Constructor
             
    }//End QuotesDetails   
}