﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using PharmaQuoteService.Classes;
using System.Net.Mail;
using System;
using PdfSharp.Pdf;
using PdfSharp.Drawing;

namespace PharmaQuoteService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service.svc or Service.svc.cs at the Solution Explorer and start debugging.
    public class Service : IService
    {
        //Retrieve Connection String By Name
        readonly string SConn = "PharmaQuoteService.Properties.Settings.DBConn";
        readonly string StrSmtp = Properties.Settings.Default.Smtp.ToString();
        readonly int Port = Properties.Settings.Default.Port;
        readonly string Subject = Properties.Settings.Default.Subject.ToString();
        readonly string Email = Properties.Settings.Default.Email.ToString();
        readonly string HtmlBody = Properties.Settings.Default.HtmlBody.ToString();
        readonly string Pass = Properties.Settings.Default.Pass.ToString();
        readonly string CC = Properties.Settings.Default.CC.ToString();

        public string Pdf()
        {
            PdfDocument document = new PdfDocument();
            document.Info.Title = "Created with PDFsharp";
            // Create an empty page
            PdfPage page = document.AddPage();
            // Get an XGraphics object for drawing
            XGraphics gfx = XGraphics.FromPdfPage(page);

            // Create a font
            XFont font = new XFont("Verdana", 20, XFontStyle.BoldItalic);
            // Draw the text
            gfx.DrawString("Hello, World!", font, XBrushes.Black,
            new XRect(0, 0, page.Width, page.Height),
            XStringFormats.Center);

            // Save the document...
            const string filename = "PDF/HelloWorld.pdf";
            document.Save(filename);
            return "Ok";
            
        }

        //Return RFQ and quotes list Sent per vendor
        public QuotesList QuotesListPerRFQ(long IdRFQ)
        {
            QuotesList Lista = new QuotesList();
            Lista.Lista = new List<Quotes>();
            Lista.Error = false;


            try
            {
                ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[SConn];
                SqlConnection conn = new SqlConnection(settings.ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = string.Format("Select VendorQuotes.*, Vendors.Name From VendorQuotes, Vendors Where VendorQuotes.IdVendor = Vendors.Id And IdRFQ = " + IdRFQ);

                SqlDataReader rS = cmd.ExecuteReader(CommandBehavior.Default);
                if (rS.HasRows)
                {
                    while (rS.Read())
                    {
                        Quotes q = new Quotes
                        {
                            Id = rS.GetInt64(0),
                            IdVendor = long.Parse(rS["IdVendor"].ToString()),
                            IdRFQs = rS.GetInt64(2),
                            Created = (DateTime)rS["Date"],
                            Status = rS.GetBoolean(4),
                            IncludedShipping = rS.GetBoolean(5),
                            ShippingCost = (float)rS["ShippingCost"],
                            VendorName = (string)rS["Name"],
                        };                        
                        Lista.Lista.Add(q);
                    }
                }
                else
                {
                    Lista.Error = true;
                    Lista.Msg = "Empty List";
                }//End if
                rS.Close();
                conn.Dispose();
            }
            catch (SqlException e)
            {
                Lista.Error = true;
                Lista.Msg = "QuotesListPerRFQ: " + e.Message.ToString();
            }



            foreach (Quotes r in Lista.Lista)
            {
                r.Details = new List<QuotesDetails>();
                ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[SConn];
                SqlConnection conn = new SqlConnection(settings.ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = string.Format("Select * from VendorQuoteDetail where IdVendorQuotes = " + r.Id);
                try
                {
                    SqlDataReader rS = cmd.ExecuteReader(CommandBehavior.Default);
                    if (rS.HasRows)
                    {
                        while (rS.Read())
                        {
                            QuotesDetails q = new QuotesDetails
                            {
                                Id = rS.GetInt64(0),
                                IdVendorQuotes = rS.GetInt64(1),
                                IdRFQDetail = rS.GetInt64(2),
                                SugestedUnitPrice = (float)rS["SugestedUnitPrice"],
                                CSPPrice = (float)rS["CSPPrice"],
                                Comments = (string)rS["Comments"],
                                AltQty = (float)rS["AltQty"],
                                AltNDC = (string)rS["AltNDC"],
                                Total = (float)rS["Total"],
                                Manufacturer = (string)rS["Manufacturer"],
                                CountryOfOrigin = (string)rS["CountryOfOrigin"],
                                Unable = rS.GetBoolean(11)

                            };                            
                            r.Details.Add(q);
                        }
                    }
                    else
                    {
                        Lista.Error = true;
                        Lista.Msg = "Empty List";
                    }//End if
                    rS.Close();
                }
                catch (SqlException e)
                {
                    Lista.Error = true;
                    Lista.Msg = "List Details: " + e.Message.ToString();
                }
                conn.Dispose();

            }
            return Lista;
        }//End QuotesListPerRFQ
        //====================================================================


        //Return Quantity of Quotes per RQFs
        public QuotesPerRFQsList QtyQuotesPerRFQs(QuotesPerRFQsParams Params)
        {
            QuotesPerRFQsList QL = new QuotesPerRFQsList();
            QL.Error = false;

            try
            {
                ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[SConn];
                SqlConnection conn = new SqlConnection(settings.ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;



                string Qty = "";
                if (Params.QtyComparator != "Between")
                {
                    Qty = "B.Qty " + Params.QtyComparator + " " + Params.QtyFrom;
                }
                else
                {
                    Qty = "B.Qty " + Params.QtyComparator + " " + Params.QtyFrom + " And " + Params.QtyTo;
                }

                cmd.CommandText = string.Format("Select A.Id, A.Solicitation, A.Due, A.Posted, B.Qty, A.Customer From " +
                                                "RFQs as A " +
                                                "Inner Join " +
                                                "( " +
                                                "    Select A.Id, Count(B.Id) as Qty From " +
                                                "    (Select * From RFQs Where " +
                                                "        Solicitation Like '%" + Params.Solicitation + "%' and " +
                                                "        Due >= '" + Params.DueFrom + "' And Due <= '" + Params.DueTo + "' And " +
                                                "        Posted >= '" + Params.PostedFrom + "' And Posted <= '" + Params.PostedTo + "' " +
                                                "    ) As A " +
                                                "    left Join " +
                                                "    VendorQuotes As B " +
                                                "    On " +
                                                "    A.id = B.IdRFQ " +
                                                "    Group By A.Id " +
                                                ") as B " +
                                                "On " +
                                                "A.Id = B.Id Where " + Qty + " Order By Solicitation");
                                                
                SqlDataReader rS = cmd.ExecuteReader(CommandBehavior.Default);
                if (rS.HasRows)
                {
                    while (rS.Read())
                    {
                        QuotesPerRFQs Obj = new QuotesPerRFQs
                        {
                            Id = rS.GetInt64(0),
                            Solicitation = (string)rS["Solicitation"],
                            Posted = (DateTime)rS["Posted"],
                            Due = (DateTime)rS["Due"],
                            Qty = (int)rS["Qty"],
                            Customer = (string)rS["Customer"]
                        };
                        QL.Lista.Add(Obj);
                    }
                }
                else
                {
                    QL.Error = true;
                    QL.Msg = "Empty List";
                }//End if
                rS.Close();
                conn.Dispose();
            }
            catch (SqlException e)
            {
                QL.Error = true;
                QL.Msg = "List QuotesPerRFQs: " + e.Message.ToString();
                //conn.Dispose();
            }
            return QL;
        }//End of QuotesPerRFQs

        //From Bool 2 Bit
        private int Bool2Bit(bool b)
        {
            if (b) return 1; else return 0;            
        }
        //Save a Quote in the Data Base        
        public string SaveQuotePerVendor(Quotes q)
        {
            string Resp = "Ok";
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[SConn];
            SqlConnection conn = new SqlConnection(settings.ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = string.Format(@"INSERT INTO [dbo].[VendorQuotes] ([IdVendor],[IdRFQ],[Date],[Status],[IncludeShipping],[ShippingCost]) VALUES 
                (" + q.IdVendor + ", " + q.IdRFQs + ", getdate(), " + Bool2Bit(q.Status) + ", " + Bool2Bit(q.IncludedShipping) + ", " + q.ShippingCost +  ")");
            try
            {
                SqlDataReader rS = cmd.ExecuteReader(CommandBehavior.Default);
            }
            catch (SqlException e)
            {
                Resp = "Error Saving Quote: " + e.ToString();
                return Resp;
            }
            conn.Dispose();


            long i = MaxQ();
            foreach (QuotesDetails qd in q.Details)
            {
                ConnectionStringSettings settings2 = ConfigurationManager.ConnectionStrings[SConn];
                SqlConnection conn2 = new SqlConnection(settings2.ConnectionString);
                conn2.Open();


                cmd.Connection = conn2;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = string.Format(@"INSERT INTO [dbo].[VendorQuoteDetail] ([IdVendorQuotes],[IdRFQDetails],[SugestedUnitPrice],[CSPPrice],[Comments],[AltQty],[AltNDC],[Total],[Manufacturer],[CountryOfOrigin],[Unable]) VALUES
                (" + i + ", " + qd.IdRFQDetail + ", " + qd.SugestedUnitPrice + ", " + qd.CSPPrice + ", '" + qd.Comments + "', " + qd.AltQty + ", '" + qd.AltNDC + "', " + qd.Total + ", '" + qd.Manufacturer + "', '" + qd.CountryOfOrigin + "', " + Bool2Bit(qd.Unable) + ")");
                try
                {
                    SqlDataReader rS = cmd.ExecuteReader(CommandBehavior.Default);
                }
                catch (SqlException e)
                {
                    Resp = "Error Saving Quote Detail: " + e.ToString();
                }
                conn2.Dispose();
            }
            return Resp;
        }//End Save Quote Per Vendor
        //====================================================================

        //Return RFQ and quotes list Sent per vendor
        public RFQsList RFQListSentPerVendor(long IdVendor)
        {
            RFQsList Lista = new RFQsList();
            Lista.Lista = new List<RFQs>();
            Lista.Error = false;


            try
            {
                ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[SConn];
                SqlConnection conn = new SqlConnection(settings.ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = string.Format("select * from (select * from RFQs where due >= getdate()) as A  join (select Id as IdQuote, IdRFQ, Date, Status, IncludeShipping, ShippingCost from VendorQuotes where idvendor = " + IdVendor + ") as B on A.Id = B.IdRFQ");

                SqlDataReader rS = cmd.ExecuteReader(CommandBehavior.Default);
                if (rS.HasRows)
                {
                    while (rS.Read())
                    {
                        Quotes q = new Quotes
                        {
                            Id = long.Parse(rS["IdQuote"].ToString()),
                            IdVendor = IdVendor,
                            IdRFQs = rS.GetInt64(0),
                            Created = (DateTime)rS["Date"],
                            Status = rS.GetBoolean(13),
                            IncludedShipping = rS.GetBoolean(14),
                            ShippingCost = (float)rS["ShippingCost"],
                        };
                        RFQs Obj = new RFQs
                        {
                            Id = rS.GetInt64(0),
                            IdUser = rS.GetInt64(1),
                            Template = (string)rS["Template"],
                            Solicitation = (string)rS["Solicitation"],
                            Posted = (DateTime)rS["Posted"],
                            Due = (DateTime)rS["Due"],
                            Customer = (string)rS["Customer"],
                            Status = (string)rS["Status"],
                            FileName = (string)rS["FileName"],
                            Created = (DateTime)rS["Created"],
                            //Items = (int)rS["Items"],
                            Sent = (DateTime)rS["Date"],
                            Q = (Quotes)q,                              
                        };
                        Lista.Lista.Add(Obj);
                    }
                }
                else
                {
                    Lista.Error = true;
                    Lista.Msg = "Empty List";
                }//End if
                rS.Close();
                conn.Dispose();
            }
            catch (SqlException e)
            {
                Lista.Error = true;
                Lista.Msg = "List Per Vendor: " + e.Message.ToString();                
            }



            foreach (RFQs r in Lista.Lista)
            {
                r.Details = new List<RFQsDetails>();
                ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[SConn];
                SqlConnection conn = new SqlConnection(settings.ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = string.Format("select * from (select * from RFQDetails where idRFQ = " + r.Id + ") as A join (select Id as IdVendorQuoteDetail, IdRFQDetails, SugestedUnitPrice, CSPPrice, Comments, AltQty, AltNDC, Total, Manufacturer, CountryOfOrigin, Unable from VendorQuoteDetail where IdVendorQuotes = " + r.Q.Id + ") as B On A.Id = B.IdRFQDetails");
                try
                {                    
                    SqlDataReader rS = cmd.ExecuteReader(CommandBehavior.Default);
                    if (rS.HasRows)
                    {
                        while (rS.Read())
                        {
                            QuotesDetails q = new QuotesDetails
                            {
                                Id = rS.GetInt64(5),
                                IdVendorQuotes = r.Q.Id,
                                IdRFQDetail = rS.GetInt64(6),
                                SugestedUnitPrice = (float)rS["SugestedUnitPrice"],
                                CSPPrice = (float)rS["CSPPrice"],
                                Comments = (string)rS["Comments"],
                                AltQty = (float)rS["AltQty"],
                                AltNDC = (string)rS["AltNDC"],
                                Total = (float)rS["Total"],
                                Manufacturer = (string)rS["Manufacturer"],
                                CountryOfOrigin = (string)rS["CountryOfOrigin"],
                                Unable = rS.GetBoolean(15)

                            };
                            RFQsDetails Obj = new RFQsDetails
                            {
                                Id = rS.GetInt64(0),
                                IdRFQ = rS.GetInt64(1),
                                NDC = (string)rS["NDC"],
                                Qty = (float)rS["Qty"],
                                Description = (string)rS["Description"]
                            };
                            Obj.QD = q;
                            r.Details.Add(Obj);
                        }
                    }
                    else
                    {
                        Lista.Error = true;
                        Lista.Msg = "Empty List";
                    }//End if
                    rS.Close();
                }
                catch (SqlException e)
                {
                    Lista.Error = true;
                    Lista.Msg = "List Details: " + e.Message.ToString();
                }
                conn.Dispose();

            }


            return Lista;
        }//End RFQListSentPerVendor
        //====================================================================

        //Return RFQ list per vendor
        public RFQsList RFQListPerVendor(long IdVendor)
        {
            RFQsList Lista = new RFQsList();
            Lista.Lista = new List<RFQs>();
            Lista.Error = false;
            
            
            try
            {
                ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[SConn];
                SqlConnection conn = new SqlConnection(settings.ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = string.Format("Select c.*, d.Items From (SELECT a.* FROM(SELECT * FROM[PharmaQuote].[dbo].[RFQs] where Due >= getdate() and " +
                    "(Status = 'Created' or Status = 'Sent')) as a LEFT JOIN(SELECT IdRFQ as ID FROM[PharmaQuote].[dbo].[VendorQuotes] where IdVendor = " + IdVendor + ") AS b ON b.ID = a.Id " +
                    "WHERE B.ID IS NULL) as c JOIN(SELECT IdRFQ, count(Id) as Items FROM[PharmaQuote].[dbo].[RFQDetails] group by IdRFQ) as d ON d.IdRFQ = c.Id");

                SqlDataReader rS = cmd.ExecuteReader(CommandBehavior.Default);
                if (rS.HasRows)
                {
                    while (rS.Read())
                    {
                        RFQs Obj = new RFQs
                        {
                            Id = rS.GetInt64(0),
                            IdUser = rS.GetInt64(1),
                            Template = (string)rS["Template"],
                            Solicitation = (string)rS["Solicitation"],
                            Posted = (DateTime)rS["Posted"],
                            Due = (DateTime)rS["Due"],
                            Customer = (string)rS["Customer"],
                            Status = (string)rS["Status"],
                            FileName = (string)rS["FileName"],
                            Created = (DateTime)rS["Created"],
                            Items = (int)rS["Items"],
                        };
                        Lista.Lista.Add(Obj);
                    }
                }
                else
                {
                    Lista.Error = true;
                    Lista.Msg = "Empty List";
                }//End if
                rS.Close();
                conn.Dispose();
            }
            catch (SqlException e)
            {
                Lista.Error = true;
                Lista.Msg = "List Per Vendor: " + e.Message.ToString();
                //conn.Dispose();
            }
            



            foreach (RFQs r in Lista.Lista)
            {
                r.Details = new List<RFQsDetails>();
                ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[SConn];
                SqlConnection conn = new SqlConnection(settings.ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;                
                cmd.CommandText = string.Format("SELECT *  FROM [PharmaQuote].[dbo].[RFQDetails] where IdRFQ = " + r.Id);
                try
                {
                    SqlDataReader rS = cmd.ExecuteReader(CommandBehavior.Default);
                    if (rS.HasRows)
                    {
                        while (rS.Read())
                        {
                            RFQsDetails Obj = new RFQsDetails
                            {
                                Id = rS.GetInt64(0),
                                IdRFQ = rS.GetInt64(1),
                                NDC = (string)rS["NDC"],
                                Qty = (float)rS["Qty"],
                                Description = (string)rS["Description"]                                
                            };
                            r.Details.Add(Obj);
                        }
                    }
                    else
                    {
                        Lista.Error = true;
                        Lista.Msg = "Empty List";
                    }//End if
                    rS.Close();
                }
                catch (SqlException e)
                {
                    Lista.Error = true;
                    Lista.Msg = "List Details: " + e.Message.ToString();
                }
                conn.Dispose();

            }

            return Lista;
        }//End RFQListPerVendor
        //====================================================================

        //Return RFQ  per Id
        public RFQsList RFQPerId(long Id)
        {

            RFQsList Lista = new RFQsList();
            Lista.Lista = new List<RFQs>();
            Lista.Error = false;


            try
            {
                ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[SConn];
                SqlConnection conn = new SqlConnection(settings.ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                /*
                 * cmd.CommandText = string.Format("Select c.*, d.Items From (SELECT a.* FROM(SELECT * FROM[PharmaQuote].[dbo].[RFQs] where Due >= getdate() and " +
                    "(Status = 'Created' or Status = 'Sent')) as a LEFT JOIN(SELECT IdRFQ as ID FROM[PharmaQuote].[dbo].[VendorQuotes] where IdVendor = " + IdVendor + ") AS b ON b.ID = a.Id " +
                    "WHERE B.ID IS NULL) as c JOIN(SELECT IdRFQ, count(Id) as Items FROM[PharmaQuote].[dbo].[RFQDetails] group by IdRFQ) as d ON d.IdRFQ = c.Id");
                */
                cmd.CommandText = string.Format("Select * From [PharmaQuote].[dbo].[RFQs] Where Id =" + Id);

                SqlDataReader rS = cmd.ExecuteReader(CommandBehavior.Default);
                if (rS.HasRows)
                {
                    while (rS.Read())
                    {
                        RFQs Obj = new RFQs
                        {
                            Id = rS.GetInt64(0),
                            IdUser = rS.GetInt64(1),
                            Template = (string)rS["Template"],
                            Solicitation = (string)rS["Solicitation"],
                            Posted = (DateTime)rS["Posted"],
                            Due = (DateTime)rS["Due"],
                            Customer = (string)rS["Customer"],
                            Status = (string)rS["Status"],
                            FileName = (string)rS["FileName"],
                            Created = (DateTime)rS["Created"],
                            //Items = (int)rS["Items"],
                        };
                        Lista.Lista.Add(Obj);
                    }
                }
                else
                {
                    Lista.Error = true;
                    Lista.Msg = "Empty List";
                }//End if
                rS.Close();
                conn.Dispose();
            }
            catch (SqlException e)
            {
                Lista.Error = true;
                Lista.Msg = "List Per Vendor: " + e.Message.ToString();
                //conn.Dispose();
            }




            foreach (RFQs r in Lista.Lista)
            {
                r.Details = new List<RFQsDetails>();
                ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[SConn];
                SqlConnection conn = new SqlConnection(settings.ConnectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = string.Format("SELECT *  FROM [PharmaQuote].[dbo].[RFQDetails] where IdRFQ = " + r.Id);
                try
                {
                    SqlDataReader rS = cmd.ExecuteReader(CommandBehavior.Default);
                    if (rS.HasRows)
                    {
                        while (rS.Read())
                        {
                            RFQsDetails Obj = new RFQsDetails
                            {
                                Id = rS.GetInt64(0),
                                IdRFQ = rS.GetInt64(1),
                                NDC = (string)rS["NDC"],
                                Qty = (float)rS["Qty"],
                                Description = (string)rS["Description"]
                            };
                            r.Details.Add(Obj);
                        }
                    }
                    else
                    {
                        Lista.Error = true;
                        Lista.Msg = "Empty List";
                    }//End if
                    rS.Close();
                }
                catch (SqlException e)
                {
                    Lista.Error = true;
                    Lista.Msg = "List Details: " + e.Message.ToString();
                }
                conn.Dispose();

            }

            return Lista;
        }//End RFQPerId
        //====================================================================

        //Get User By UserName and PassWord
        public VendorUser VendorUserByPass(string UserName, string Password)
        {
            VendorUser Vu = new VendorUser();
            Vu.Status = false;
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[SConn];
            SqlConnection conn = new SqlConnection(settings.ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;

            //cmd.CommandText = string.Format("SELECT * FROM [dbo].[VendorUsers] WHERE UserName = '"+ UserName + "' and Password = CONVERT(VARCHAR(32), HashBytes('MD5', '" + Password + "'), 2)");
            cmd.CommandText = string.Format("SELECT A.*, B.Name as VendorName FROM[PharmaQuote].[dbo].[VendorUsers] as A, Vendors as B where A.IdVendor = b.Id and(UserName = '" + UserName + "' and Password = CONVERT(VARCHAR(32), HashBytes('MD5', '" + Password + "'), 2))");
            try
            {
                SqlDataReader rS = cmd.ExecuteReader(CommandBehavior.Default);
                if (rS.HasRows)
                {
                    while (rS.Read())
                    {
                        VendorUser Obj = new VendorUser
                        {
                            Id = rS.GetInt64(0),
                            IdVendor = rS.GetInt64(1),
                            UserName = (string)rS["UserName"],
                            Password = (string)rS["Password"],
                            Email = (string)rS["Email"],
                            Phone = (string)rS["Phone"],
                            IdType = rS.GetInt64(6),
                            Status = rS.GetBoolean(9),
                            FirstName = (string)rS["FirstName"],
                            LastName = (string)rS["LastName"],
                            Receiver = rS.GetBoolean(10),
                            VendorName = (string)rS["VendorName"]
                        };
                        Vu = Obj;
                    }
                }
                else
                {
                    Vu.Error = true;
                    Vu.Msg = "User and password no match..";
                }//End if
                rS.Close();
            }
            catch (SqlException e)
            {
                Vu.Error = true;
                Vu.Msg = "VendroUserByPass: " + e.Message.ToString();                
            }
            conn.Dispose();

            return Vu;
        }//End Vendor User By Pass
        //====================================================================

        //Send Emails
        public string SendEmails(string email, string name, string SN)
        {
            string resp = "Ok";
            try
            {
                //SmtpClient SmtpServer = new SmtpClient("smtp.live.com");                
                SmtpClient SmtpServer = new SmtpClient(StrSmtp);
                var mail = new MailMessage();
                mail.From = new MailAddress(Email);
                mail.To.Add(email);
                mail.Bcc.Add(CC);
                mail.Subject = Subject.Replace("#SN#",SN);
                mail.IsBodyHtml = true;
                //string htmlBody;
                //htmlBody = "Write some HTML code here";
                mail.Body = HtmlBody.Replace("#NAME#",name);                
                SmtpServer.Port = Port;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new System.Net.NetworkCredential(Email, Pass);
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
            }catch(Exception e)
            {
                resp = e.ToString();
            }
            return resp;
        }//End Send Emails
        //====================================================================

        //Save RFQs in to a Data Base here we send the massive email
        public string SaveRFQs( RFQs Rf)
        {
            string Resp = "Ok";

            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[SConn];
            SqlConnection conn = new SqlConnection(settings.ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = string.Format(@"INSERT INTO [dbo].[RFQs]([IdUser], [Template], [Solicitation], [Posted], [Due], [Customer], [Status], [FileName], [Created]) VALUES  
                (" + Rf.IdUser + ",'" + Rf.Template + "','" + Rf.Solicitation + "','" + Rf.Posted + "','" + Rf.Due + "','" + Rf.Customer + "','" + Rf.Status + "','" + Rf.FileName + "','" + Rf.Created + "')");
            try
            {
                SqlDataReader rS = cmd.ExecuteReader(CommandBehavior.Default);
            }
            catch (SqlException e)
            {
                Resp = "Error Saving RFQs: " + e.ToString();
                return Resp;
            }
            conn.Dispose();


            long i = MaxRFQS();
            foreach (RFQsDetails RD in Rf.Details)
            {
                ConnectionStringSettings settings2 = ConfigurationManager.ConnectionStrings[SConn];
                SqlConnection conn2 = new SqlConnection(settings2.ConnectionString);
                conn2.Open();


                cmd.Connection = conn2;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = string.Format("INSERT INTO [dbo].[RFQDetails]([IdRFQ],[NDC],[Qty],[Description])VALUES(" + i + ",'" + RD.NDC + "','" + RD.Qty + "','" + RD.Description + "')");
                try
                {
                    SqlDataReader rS = cmd.ExecuteReader(CommandBehavior.Default);
                }
                catch (SqlException e)
                {
                    Resp = "Error Saving RFQs: " + e.ToString();
                }
                conn2.Dispose();
            }

            //Preparing for masive email
            ConnectionStringSettings settings3 = ConfigurationManager.ConnectionStrings[SConn];
            SqlConnection conn3 = new SqlConnection(settings3.ConnectionString);
            conn3.Open();


            cmd.Connection = conn3;
            cmd.CommandType = CommandType.Text;
            //cmd.CommandText = string.Format("SELECT * FROM [dbo].[VendorUsers] WHERE Status = 1 AND Receiver = 1");
            cmd.CommandText = string.Format("SELECT A.*, B.Name as VendorName FROM[PharmaQuote].[dbo].[VendorUsers] as A, Vendors as B where A.IdVendor = b.Id and A.Status = 1 and A.Receiver = 1");
            
            VendorUserList vul = new VendorUserList();
            vul.List = new List<VendorUser>();
            try
            {
                SqlDataReader rS = cmd.ExecuteReader(CommandBehavior.Default);
                if (rS.HasRows)
                {
                    while (rS.Read())
                    {
                        VendorUser Obj = new VendorUser
                        {
                            Id = rS.GetInt64(0),
                            IdVendor = rS.GetInt64(1),
                            UserName = (string)rS["UserName"],
                            Password = (string)rS["Password"],
                            Email = (string)rS["Email"],
                            Phone = (string)rS["Phone"],
                            IdType = rS.GetInt64(6),
                            Status = true,
                            FirstName = (string)rS["FirstName"],
                            LastName = (string)rS["LastName"],
                            VendorName = (string)rS["VendorName"],
                            Receiver = true
                        };
                        vul.List.Add(Obj);
                    }
                }
                else
                {
                    vul.Error = false;
                    vul.Msg = "No templates Aviables";
                }//End if
                rS.Close();
            }
            catch (SqlException e)
            {
                Resp = "loading emails" + e.ToString();
            }
            conn3.Dispose();


            foreach (VendorUser vu in vul.List)
            {
                SendEmails(vu.Email, vu.VendorName, Rf.Solicitation);
            }

            return Resp;
        }//End SaveRFQs
         //====================================================================

        //Return max Id VendorQuote
        private long MaxQ()
        {
            long i = 0;

            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[SConn];
            SqlConnection conn = new SqlConnection(settings.ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = string.Format("SELECT max(id) Id FROM[PharmaQuote].[dbo].[VendorQuotes]");
            try
            {
                SqlDataReader rS = cmd.ExecuteReader(CommandBehavior.Default);

                if (rS.HasRows)
                {
                    while (rS.Read())
                    {
                        i = long.Parse(rS["Id"].ToString());
                    }
                }
                rS.Close();
            }
            catch (SqlException e)
            {
                i = -1;
            }
            conn.Dispose();
            return i;

        }//End MaxQ
        //====================================================================

        //Return max Id RFQS
        private long MaxRFQS()
        {
            long i = 0;

            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[SConn];
            SqlConnection conn = new SqlConnection(settings.ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = string.Format("SELECT max(id) Id FROM[PharmaQuote].[dbo].[RFQs]");
            try
            {
                SqlDataReader rS = cmd.ExecuteReader(CommandBehavior.Default);

                if (rS.HasRows)
                {
                    while (rS.Read())
                    {
                        i = long.Parse(rS["Id"].ToString());
                    }
                }
                rS.Close();
            }
            catch (SqlException e)
            {
                i = -1;
            }
            conn.Dispose();
            return i;

        }//End MaxRFQs
        //====================================================================

        //CheckRFQ check is the SN exist in the data base
        public long CheckRFQ(string SN)
        {            
            long i = 0;

            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[SConn];
            SqlConnection conn = new SqlConnection(settings.ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = string.Format("SELECT ISNULL(count(id), 0 )+0 as Exist FROM [PharmaQuote].[dbo].[RFQs] where Solicitation = '" + SN + "' and status <> 'Closed' and status <> 'Canceled' and status <> 'Due'");
            try
            {
                SqlDataReader rS = cmd.ExecuteReader(CommandBehavior.Default);

                if (rS.HasRows)
                {                    
                    while (rS.Read())
                    {                        
                        i = long.Parse(rS["Exist"].ToString());
                    }
                }                
                rS.Close();
            }
            catch (SqlException e)
            {
                i = -1;
            }
            conn.Dispose();
            return i;
        }//End CheckRFQ
        //====================================================================

        //Add a new template to the data base
        public string AddTemplate(Templates t)
        {
            string Resp = "Template Added";

            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[SConn];
            SqlConnection conn = new SqlConnection(settings.ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = string.Format("INSERT INTO [dbo].[Templates] ([Name],[Template]) VALUES ('" + t.Name + "','" + t.Template + "')");
            try
            {
                SqlDataReader rS = cmd.ExecuteReader(CommandBehavior.Default);
            }
            catch (SqlException e)
            {
                Resp = "Error Add Template: " + e.ToString() ;
            }
            conn.Dispose();

            return Resp;
        }//End Add Template
        //====================================================================
        
        //Load  Templates
        public TemplatesList Templates_List()
        {
            TemplatesList Lista = new TemplatesList
            {
                List = new List<Templates>()
            };

            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[SConn];
            SqlConnection conn = new SqlConnection(settings.ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            
            cmd.CommandText = string.Format("Select * from  [dbo].[Templates]");
            try
            {
                SqlDataReader rS = cmd.ExecuteReader(CommandBehavior.Default);

                if (rS.HasRows)
                {
                    while (rS.Read())
                    {
                        Templates Obj = new Templates
                        {
                            Id = rS.GetInt64(0),
                            Name = (string)rS["Name"],
                            Template = (string)rS["Template"],
                        };                        
                        Lista.List.Add(Obj);
                    }
                }
                else
                {
                    Lista.Error = false;
                    Lista.Msg = "No templates Aviables";
                }//End if
                rS.Close();
            }
            catch (SqlException e)
            {
                Lista.Error = true;
                Lista.Msg = "Error Service Templates_List:" + e.ToString();
            }
            conn.Dispose();
            
            return Lista;
        }//End Template List
        //====================================================================

    }//End Class    
}
