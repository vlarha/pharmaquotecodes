﻿namespace PharmaQuote
{
    partial class FullForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MenuContainer = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.PanelLoad = new System.Windows.Forms.Panel();
            this.LblLoad = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LblMenu = new System.Windows.Forms.Label();
            this.Contenedor = new System.Windows.Forms.GroupBox();
            this.LblTittle = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.MenuContainer.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.PanelLoad.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuContainer
            // 
            this.MenuContainer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.MenuContainer.BackColor = System.Drawing.SystemColors.Highlight;
            this.MenuContainer.Controls.Add(this.panel4);
            this.MenuContainer.Controls.Add(this.panel2);
            this.MenuContainer.Controls.Add(this.panel3);
            this.MenuContainer.Controls.Add(this.PanelLoad);
            this.MenuContainer.Controls.Add(this.panel1);
            this.MenuContainer.Location = new System.Drawing.Point(0, -6);
            this.MenuContainer.Name = "MenuContainer";
            this.MenuContainer.Size = new System.Drawing.Size(171, 827);
            this.MenuContainer.TabIndex = 0;
            this.MenuContainer.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(6, 142);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(159, 43);
            this.panel2.TabIndex = 8;
            this.panel2.Tag = "Vendors";
            this.panel2.Click += new System.EventHandler(this.Lbl_Click);
            this.panel2.MouseEnter += new System.EventHandler(this.Lbl_MouseEnter);
            this.panel2.MouseLeave += new System.EventHandler(this.Lbl_MouseLeave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label1.Location = new System.Drawing.Point(4, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 25);
            this.label1.TabIndex = 5;
            this.label1.Tag = "Vendors";
            this.label1.Text = "Vendors";
            this.label1.Click += new System.EventHandler(this.Lbl_Click);
            this.label1.MouseEnter += new System.EventHandler(this.Lbl_MouseEnter);
            this.label1.MouseLeave += new System.EventHandler(this.Lbl_MouseLeave);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(6, 93);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(159, 43);
            this.panel3.TabIndex = 6;
            this.panel3.Tag = "Quotes";
            this.panel3.Click += new System.EventHandler(this.Lbl_Click);
            this.panel3.MouseEnter += new System.EventHandler(this.Lbl_MouseEnter);
            this.panel3.MouseLeave += new System.EventHandler(this.Lbl_MouseLeave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label3.Location = new System.Drawing.Point(4, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 25);
            this.label3.TabIndex = 5;
            this.label3.Tag = "Quotes";
            this.label3.Text = "View Quotes";
            this.label3.Click += new System.EventHandler(this.Lbl_Click);
            this.label3.MouseEnter += new System.EventHandler(this.Lbl_MouseEnter);
            this.label3.MouseLeave += new System.EventHandler(this.Lbl_MouseLeave);
            // 
            // PanelLoad
            // 
            this.PanelLoad.BackColor = System.Drawing.SystemColors.Highlight;
            this.PanelLoad.Controls.Add(this.LblLoad);
            this.PanelLoad.Location = new System.Drawing.Point(6, 44);
            this.PanelLoad.Name = "PanelLoad";
            this.PanelLoad.Size = new System.Drawing.Size(159, 43);
            this.PanelLoad.TabIndex = 5;
            this.PanelLoad.Tag = "Load";
            this.PanelLoad.Click += new System.EventHandler(this.Lbl_Click);
            this.PanelLoad.MouseEnter += new System.EventHandler(this.Lbl_MouseEnter);
            this.PanelLoad.MouseLeave += new System.EventHandler(this.Lbl_MouseLeave);
            // 
            // LblLoad
            // 
            this.LblLoad.AutoSize = true;
            this.LblLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLoad.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.LblLoad.Location = new System.Drawing.Point(4, 9);
            this.LblLoad.Name = "LblLoad";
            this.LblLoad.Size = new System.Drawing.Size(110, 25);
            this.LblLoad.TabIndex = 5;
            this.LblLoad.Tag = "Load";
            this.LblLoad.Text = "Load File";
            this.LblLoad.Click += new System.EventHandler(this.Lbl_Click);
            this.LblLoad.MouseEnter += new System.EventHandler(this.Lbl_MouseEnter);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel1.Controls.Add(this.LblMenu);
            this.panel1.Location = new System.Drawing.Point(0, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(171, 32);
            this.panel1.TabIndex = 3;
            // 
            // LblMenu
            // 
            this.LblMenu.AutoSize = true;
            this.LblMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblMenu.Location = new System.Drawing.Point(4, 4);
            this.LblMenu.Name = "LblMenu";
            this.LblMenu.Size = new System.Drawing.Size(70, 25);
            this.LblMenu.TabIndex = 1;
            this.LblMenu.Text = "Menu";
            // 
            // Contenedor
            // 
            this.Contenedor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Contenedor.Location = new System.Drawing.Point(177, 33);
            this.Contenedor.Name = "Contenedor";
            this.Contenedor.Size = new System.Drawing.Size(1126, 788);
            this.Contenedor.TabIndex = 1;
            this.Contenedor.TabStop = false;
            // 
            // LblTittle
            // 
            this.LblTittle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LblTittle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTittle.Location = new System.Drawing.Point(1062, 9);
            this.LblTittle.Name = "LblTittle";
            this.LblTittle.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblTittle.Size = new System.Drawing.Size(235, 25);
            this.LblTittle.TabIndex = 2;
            this.LblTittle.Text = "Load File";
            this.LblTittle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Highlight;
            this.panel4.Controls.Add(this.label2);
            this.panel4.Location = new System.Drawing.Point(6, 191);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(159, 43);
            this.panel4.TabIndex = 9;
            this.panel4.Tag = "Other";
            this.panel4.Click += new System.EventHandler(this.Lbl_Click);
            this.panel4.MouseEnter += new System.EventHandler(this.Lbl_MouseEnter);
            this.panel4.MouseLeave += new System.EventHandler(this.Lbl_MouseLeave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label2.Location = new System.Drawing.Point(4, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 25);
            this.label2.TabIndex = 5;
            this.label2.Tag = "Other";
            this.label2.Text = "Other";
            this.label2.Click += new System.EventHandler(this.Lbl_Click);
            this.label2.MouseEnter += new System.EventHandler(this.Lbl_MouseEnter);
            this.label2.MouseLeave += new System.EventHandler(this.Lbl_MouseLeave);
            // 
            // FullForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1309, 823);
            this.Controls.Add(this.LblTittle);
            this.Controls.Add(this.Contenedor);
            this.Controls.Add(this.MenuContainer);
            this.Name = "FullForm";
            this.Text = "Solicitations";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.MenuContainer.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.PanelLoad.ResumeLayout(false);
            this.PanelLoad.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox MenuContainer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label LblMenu;
        private System.Windows.Forms.Panel PanelLoad;
        private System.Windows.Forms.Label LblLoad;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox Contenedor;
        private System.Windows.Forms.Label LblTittle;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
    }
}