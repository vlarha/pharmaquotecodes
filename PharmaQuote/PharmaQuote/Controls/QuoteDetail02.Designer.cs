﻿namespace PharmaQuote.Controls
{
    partial class QuoteDetail02
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblQuotes = new System.Windows.Forms.Label();
            this.LblPosted = new System.Windows.Forms.Label();
            this.LblDueDate = new System.Windows.Forms.Label();
            this.LblRFQ = new System.Windows.Forms.Label();
            this.BtnView = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LblQuotes
            // 
            this.LblQuotes.AutoSize = true;
            this.LblQuotes.Location = new System.Drawing.Point(320, 12);
            this.LblQuotes.Name = "LblQuotes";
            this.LblQuotes.Size = new System.Drawing.Size(25, 13);
            this.LblQuotes.TabIndex = 8;
            this.LblQuotes.Text = "000";
            // 
            // LblPosted
            // 
            this.LblPosted.AutoSize = true;
            this.LblPosted.Location = new System.Drawing.Point(223, 12);
            this.LblPosted.Name = "LblPosted";
            this.LblPosted.Size = new System.Drawing.Size(66, 13);
            this.LblPosted.TabIndex = 7;
            this.LblPosted.Text = "Posted Date";
            // 
            // LblDueDate
            // 
            this.LblDueDate.AutoSize = true;
            this.LblDueDate.Location = new System.Drawing.Point(139, 12);
            this.LblDueDate.Name = "LblDueDate";
            this.LblDueDate.Size = new System.Drawing.Size(53, 13);
            this.LblDueDate.TabIndex = 6;
            this.LblDueDate.Text = "Due Date";
            // 
            // LblRFQ
            // 
            this.LblRFQ.AutoSize = true;
            this.LblRFQ.Location = new System.Drawing.Point(3, 12);
            this.LblRFQ.Name = "LblRFQ";
            this.LblRFQ.Size = new System.Drawing.Size(29, 13);
            this.LblRFQ.TabIndex = 5;
            this.LblRFQ.Text = "RFQ";
            // 
            // BtnView
            // 
            this.BtnView.Location = new System.Drawing.Point(347, 8);
            this.BtnView.Name = "BtnView";
            this.BtnView.Size = new System.Drawing.Size(23, 21);
            this.BtnView.TabIndex = 9;
            this.BtnView.Text = ">";
            this.BtnView.UseVisualStyleBackColor = true;
            this.BtnView.Click += new System.EventHandler(this.BtnView_Click);
            // 
            // QuoteDetail02
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.BtnView);
            this.Controls.Add(this.LblQuotes);
            this.Controls.Add(this.LblPosted);
            this.Controls.Add(this.LblDueDate);
            this.Controls.Add(this.LblRFQ);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "QuoteDetail02";
            this.Size = new System.Drawing.Size(375, 36);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LblQuotes;
        private System.Windows.Forms.Label LblPosted;
        private System.Windows.Forms.Label LblDueDate;
        private System.Windows.Forms.Label LblRFQ;
        private System.Windows.Forms.Button BtnView;
    }
}
