﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PharmaQuote.Controls
{
    public partial class QuoteDetail01 : UserControl
    {
        private Quotes MyQuotes;
        public QuoteDetail01()
        {
            InitializeComponent();            
        }
        public void SetMyQUotes(Quotes myquotes)
        {
            MyQuotes = (Quotes)myquotes;
        }

        private void LbEnter(object sender, EventArgs e)
        {
            Label Lb = (Label)sender;
            Lb.ForeColor = Color.Blue;            
        }

        private void LbLeave(object sender, EventArgs e)
        {
            Label Lb = (Label)sender;
            Lb.ForeColor = Color.Black;            
        }
        

        private void Pic_Click(object sender, EventArgs e)
        {
            InvisibleAll();
            PictureBox pb = (PictureBox)sender;
            pb.Visible = true;            
            if (pb.Tag.ToString()=="Down")
            {
                pb.Image = pb.ErrorImage;
                pb.Tag = "Up";
            }
            else
            {
                pb.Image = pb.InitialImage;
                pb.Tag = "Down";
            }

            MyQuotes.SortBy(pb.Name, pb.Tag.ToString());

        }
        private void InvisibleAll()
        {
            PicRFQ.Visible = false;
            PicDue.Visible = false;
            PicPosted.Visible = false;
            PicQuotes.Visible = false;
        }


        public void DefaultSort()
        {
            InvisibleAll();
            PictureBox pb = (PictureBox)PicDue;
            pb.Visible = true;
            pb.Image = pb.ErrorImage;
            pb.Tag = "Up";
            MyQuotes.SortBy(pb.Name, pb.Tag.ToString());
        }

        private void LblDueDate_Click(object sender, EventArgs e)
        {
            Pic_Click(PicDue, new EventArgs());
        }

        private void LblRFQ_Click(object sender, EventArgs e)
        {
            Pic_Click(PicRFQ, new EventArgs());
        }

        private void LblPosted_Click(object sender, EventArgs e)
        {
            Pic_Click(PicPosted, new EventArgs());
        }

        private void LblQuotes_Click(object sender, EventArgs e)
        {
            Pic_Click(PicQuotes, new EventArgs());
        }
    }
}
