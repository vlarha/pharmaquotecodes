﻿using PharmaQuote.ServiceReference;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace PharmaQuote.Controls
{
    public partial class QuoteDetail02 : UserControl
    {
        public QuotesPerRFQs QPR;
        private string DateFormat = "MM/dd/yyyy";
        private Quotes QT;

        public QuoteDetail02(QuotesPerRFQs qpr,Quotes qt)
        {
            InitializeComponent();
            QPR = qpr;
            QT = qt;
            SetValues();
        }

        private void  SetValues()
        {            
            LblRFQ.Text = QPR.Solicitation;
            LblDueDate.Text = QPR.Due.Date.ToString(DateFormat);
            LblPosted.Text = QPR.Posted.Date.ToString(DateFormat);
            LblQuotes.Text = QPR.Qty.ToString();

            if (QPR.Due.Date < DateTime.Now.AddDays(5)) LblDueDate.ForeColor = Color.Orange;
            if (QPR.Due.Date < DateTime.Now)  LblDueDate.ForeColor = Color.Red;
            

        }

        private void BtnView_Click(object sender, EventArgs e)
        {
            QT.ShowQuotesPerRFQs(QPR);
        }
    }
}
