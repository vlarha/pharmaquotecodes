﻿namespace PharmaQuote.Controls
{
    partial class QuoteDetail01
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuoteDetail01));
            this.LblRFQ = new System.Windows.Forms.Label();
            this.LblDueDate = new System.Windows.Forms.Label();
            this.LblPosted = new System.Windows.Forms.Label();
            this.LblQuotes = new System.Windows.Forms.Label();
            this.PicRFQ = new System.Windows.Forms.PictureBox();
            this.PicDue = new System.Windows.Forms.PictureBox();
            this.PicPosted = new System.Windows.Forms.PictureBox();
            this.PicQuotes = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PicRFQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicPosted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicQuotes)).BeginInit();
            this.SuspendLayout();
            // 
            // LblRFQ
            // 
            this.LblRFQ.AutoSize = true;
            this.LblRFQ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblRFQ.Location = new System.Drawing.Point(21, 12);
            this.LblRFQ.Name = "LblRFQ";
            this.LblRFQ.Size = new System.Drawing.Size(29, 13);
            this.LblRFQ.TabIndex = 0;
            this.LblRFQ.Text = "RFQ";
            this.LblRFQ.Click += new System.EventHandler(this.LblRFQ_Click);
            this.LblRFQ.MouseEnter += new System.EventHandler(this.LbEnter);
            this.LblRFQ.MouseLeave += new System.EventHandler(this.LbLeave);
            // 
            // LblDueDate
            // 
            this.LblDueDate.AutoSize = true;
            this.LblDueDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDueDate.Location = new System.Drawing.Point(139, 12);
            this.LblDueDate.Name = "LblDueDate";
            this.LblDueDate.Size = new System.Drawing.Size(53, 13);
            this.LblDueDate.TabIndex = 2;
            this.LblDueDate.Text = "Due Date";
            this.LblDueDate.Click += new System.EventHandler(this.LblDueDate_Click);
            this.LblDueDate.MouseEnter += new System.EventHandler(this.LbEnter);
            this.LblDueDate.MouseLeave += new System.EventHandler(this.LbLeave);
            // 
            // LblPosted
            // 
            this.LblPosted.AutoSize = true;
            this.LblPosted.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPosted.Location = new System.Drawing.Point(218, 12);
            this.LblPosted.Name = "LblPosted";
            this.LblPosted.Size = new System.Drawing.Size(66, 13);
            this.LblPosted.TabIndex = 3;
            this.LblPosted.Text = "Posted Date";
            this.LblPosted.Click += new System.EventHandler(this.LblPosted_Click);
            this.LblPosted.MouseEnter += new System.EventHandler(this.LbEnter);
            this.LblPosted.MouseLeave += new System.EventHandler(this.LbLeave);
            // 
            // LblQuotes
            // 
            this.LblQuotes.AutoSize = true;
            this.LblQuotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblQuotes.Location = new System.Drawing.Point(308, 12);
            this.LblQuotes.Name = "LblQuotes";
            this.LblQuotes.Size = new System.Drawing.Size(41, 13);
            this.LblQuotes.TabIndex = 4;
            this.LblQuotes.Text = "Quotes";
            this.LblQuotes.Click += new System.EventHandler(this.LblQuotes_Click);
            this.LblQuotes.MouseEnter += new System.EventHandler(this.LbEnter);
            this.LblQuotes.MouseLeave += new System.EventHandler(this.LbLeave);
            // 
            // PicRFQ
            // 
            this.PicRFQ.ErrorImage = ((System.Drawing.Image)(resources.GetObject("PicRFQ.ErrorImage")));
            this.PicRFQ.Image = ((System.Drawing.Image)(resources.GetObject("PicRFQ.Image")));
            this.PicRFQ.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicRFQ.InitialImage")));
            this.PicRFQ.Location = new System.Drawing.Point(49, 6);
            this.PicRFQ.Name = "PicRFQ";
            this.PicRFQ.Size = new System.Drawing.Size(18, 26);
            this.PicRFQ.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicRFQ.TabIndex = 5;
            this.PicRFQ.TabStop = false;
            this.PicRFQ.Tag = "Down";
            this.PicRFQ.Click += new System.EventHandler(this.Pic_Click);
            // 
            // PicDue
            // 
            this.PicDue.ErrorImage = ((System.Drawing.Image)(resources.GetObject("PicDue.ErrorImage")));
            this.PicDue.Image = ((System.Drawing.Image)(resources.GetObject("PicDue.Image")));
            this.PicDue.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicDue.InitialImage")));
            this.PicDue.Location = new System.Drawing.Point(192, 6);
            this.PicDue.Name = "PicDue";
            this.PicDue.Size = new System.Drawing.Size(18, 26);
            this.PicDue.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicDue.TabIndex = 6;
            this.PicDue.TabStop = false;
            this.PicDue.Tag = "Down";
            this.PicDue.Visible = false;
            this.PicDue.Click += new System.EventHandler(this.Pic_Click);
            // 
            // PicPosted
            // 
            this.PicPosted.ErrorImage = ((System.Drawing.Image)(resources.GetObject("PicPosted.ErrorImage")));
            this.PicPosted.Image = ((System.Drawing.Image)(resources.GetObject("PicPosted.Image")));
            this.PicPosted.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicPosted.InitialImage")));
            this.PicPosted.Location = new System.Drawing.Point(283, 6);
            this.PicPosted.Name = "PicPosted";
            this.PicPosted.Size = new System.Drawing.Size(18, 26);
            this.PicPosted.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicPosted.TabIndex = 7;
            this.PicPosted.TabStop = false;
            this.PicPosted.Tag = "Down";
            this.PicPosted.Visible = false;
            this.PicPosted.Click += new System.EventHandler(this.Pic_Click);
            // 
            // PicQuotes
            // 
            this.PicQuotes.ErrorImage = ((System.Drawing.Image)(resources.GetObject("PicQuotes.ErrorImage")));
            this.PicQuotes.Image = ((System.Drawing.Image)(resources.GetObject("PicQuotes.Image")));
            this.PicQuotes.InitialImage = ((System.Drawing.Image)(resources.GetObject("PicQuotes.InitialImage")));
            this.PicQuotes.Location = new System.Drawing.Point(348, 6);
            this.PicQuotes.Name = "PicQuotes";
            this.PicQuotes.Size = new System.Drawing.Size(18, 26);
            this.PicQuotes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicQuotes.TabIndex = 8;
            this.PicQuotes.TabStop = false;
            this.PicQuotes.Tag = "Down";
            this.PicQuotes.Visible = false;
            this.PicQuotes.Click += new System.EventHandler(this.Pic_Click);
            // 
            // QuoteDetail01
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.PicQuotes);
            this.Controls.Add(this.PicPosted);
            this.Controls.Add(this.PicDue);
            this.Controls.Add(this.PicRFQ);
            this.Controls.Add(this.LblQuotes);
            this.Controls.Add(this.LblPosted);
            this.Controls.Add(this.LblDueDate);
            this.Controls.Add(this.LblRFQ);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "QuoteDetail01";
            this.Size = new System.Drawing.Size(400, 34);
            ((System.ComponentModel.ISupportInitialize)(this.PicRFQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicPosted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicQuotes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LblRFQ;
        private System.Windows.Forms.Label LblDueDate;
        private System.Windows.Forms.Label LblPosted;
        private System.Windows.Forms.Label LblQuotes;
        private System.Windows.Forms.PictureBox PicRFQ;
        private System.Windows.Forms.PictureBox PicDue;
        private System.Windows.Forms.PictureBox PicPosted;
        private System.Windows.Forms.PictureBox PicQuotes;
    }
}
