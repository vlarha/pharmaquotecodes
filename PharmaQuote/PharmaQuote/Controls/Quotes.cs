﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PharmaQuote.ServiceReference;

namespace PharmaQuote.Controls
{
    public partial class Quotes : UserControl
    {        
        private QuotesPerRFQs eQPR;
        QuotesPerRFQsList lq = new QuotesPerRFQsList();
        public Quotes()
        {
            InitializeComponent();
            quoteDetail01.SetMyQUotes(this);
            DateDueFrom.Value = DateTime.Now;
            DateDueTo.Value = DateTime.Now.AddDays(90);
            DatePostedFrom.Value = DateTime.Now.AddDays(-90);
            DatePostedTo.Value = DateTime.Now;
        }

        public void SortBy(string str, string Direction)
        {
            switch (str)
            {                
                case "PicRFQ":
                    if (Direction == "Up") lq.Lista.Sort((x, y) => x.Solicitation.CompareTo(y.Solicitation)); else lq.Lista.Sort((y, x) => x.Solicitation.CompareTo(y.Solicitation));
                    break;
                case "PicDue":
                    if (Direction == "Up") lq.Lista.Sort((x, y) => x.Due.CompareTo(y.Due)); else lq.Lista.Sort((y, x) => x.Due.CompareTo(y.Due));
                    break;
                case "PicPosted":
                    if (Direction == "Up") lq.Lista.Sort((x, y) => x.Posted.CompareTo(y.Posted)); else lq.Lista.Sort((y, x) => x.Posted.CompareTo(y.Posted));
                    break;
                case "PicQuotes":
                    if (Direction == "Up") lq.Lista.Sort((x, y) => x.Qty.CompareTo(y.Qty)); else lq.Lista.Sort((y, x) => x.Qty.CompareTo(y.Qty));
                    break;
                default:
                    break;
            }

            ShowList();            
        }


        private void ShowList()
        {
            flowLayoutPanel1.Controls.Clear();
            int i = 0;
            foreach (QuotesPerRFQs q in lq.Lista)
            {
                QuoteDetail02 q2 = new QuoteDetail02(q,this);
                if (i % 2 == 0) q2.BackColor = Color.LightGray; else q2.BackColor = Color.White;
                flowLayoutPanel1.Controls.Add(q2);
                i++;
            }
        }
        private void Quotes_Load(object sender, EventArgs e)
        {
            Cmb.SelectedIndex = 2;                      
        }//End Quotes Load

        private Random gen = new Random();
        DateTime RandomDay()
        {
            DateTime start = new DateTime(1995, 1, 1);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(gen.Next(range));
        }

        private void Cmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (cb.SelectedItem.ToString() == "Between")
            {
                LblQuotes.Visible = true;
                TxtQuotes1.Visible = true;
                TxtQuotes1.Text = "10";
            }
            else
            {
                LblQuotes.Visible = false;
                TxtQuotes1.Visible = false;
                TxtQuotes1.Text = "";
            }
        }

        private void TxtQuotes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }            
        }


        private void Search()
        {
            Cursor.Current = Cursors.WaitCursor;
            QuotesPerRFQsParams Params = new QuotesPerRFQsParams();
            Params.Solicitation = TxtRFQ.Text;
            Params.DueFrom = DateDueFrom.Value.Date;
            Params.DueTo = DateDueTo.Value.Date;
            Params.PostedFrom = DatePostedFrom.Value.Date;
            Params.PostedTo = DatePostedTo.Value.Date;
            Params.QtyComparator = Cmb.SelectedItem.ToString();
            try { Params.QtyFrom = Int32.Parse(TxtQuotes.Text); } catch (Exception e) { Params.QtyFrom = 0; }
            try { Params.QtyTo = Int32.Parse(TxtQuotes1.Text); } catch (Exception e) { Params.QtyTo = 0; }            

            ServiceClient Sc = new ServiceClient();
            try
            {
                lq = Sc.QtyQuotesPerRFQs(Params);
                if (lq.Error)
                {
                    MessageBox.Show(lq.Msg);
                }
                ShowList();
                LblCount.Text = "" + lq.Lista.Count();
                quoteDetail01.DefaultSort();
            } catch (Exception e)
            {
                MessageBox.Show("Communication error, please try again");
            }
            
            Cursor.Current = Cursors.Default;
            }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            BtnSearch.Text = "Search";
            Search();            
        }

        private void ValueChanged(object sender, EventArgs e)
        {
            BtnSearch.Text = "* Search";
        }

        public void ShowQuotesPerRFQs(QuotesPerRFQs QPR)
        {
            eQPR = (QuotesPerRFQs) QPR;
            rfQs011.ShowQuotesPerRFQs(QPR);
            RdoBy_CheckedChanged(new object(), new EventArgs());
            //if (RdoVendor.Checked) ShowVendor(QPR);

        }

        private void ShowVendor(QuotesPerRFQs QPR)
        {
            int i = 3;
            DGV.Rows.Clear();
            DGV.Refresh();
            DGV.RowCount = 4;
            DGV.ColumnCount = 15;
            DGV.Rows[0].Cells[0].Value = "Solicitation:";
            DGV.Rows[0].Cells[1].Value = QPR.Solicitation;

            DGV.Rows[2].Cells[1].Value = "Vendor Name";
            DGV.Rows[2].Cells[2].Value = "NDC";
            DGV.Rows[2].Cells[3].Value = "Description";
            DGV.Rows[2].Cells[4].Value = "Qty";
            DGV.Rows[2].Cells[5].Value = "Alt NDC";
            DGV.Rows[2].Cells[6].Value = "Manufacturer";
            DGV.Rows[2].Cells[7].Value = "Country of Origin";
            DGV.Rows[2].Cells[8].Value = "Qty to be provided";
            DGV.Rows[2].Cells[9].Value = "Suggested Price";
            DGV.Rows[2].Cells[10].Value = "CSP Cost per Item";
            DGV.Rows[2].Cells[11].Value = "Total";
            DGV.Rows[2].Cells[12].Value = "Comments";
            DGV.Rows[2].Cells[13].Value = "Shipping Cost Included?";
            DGV.Rows[2].Cells[14].Value = "Shipping Cost";

            DGV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DGV.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DGV.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DGV.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DGV.Columns[11].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DGV.Columns[14].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            ServiceClient Sc = new ServiceClient();
            try
            {
                QuotesList QL = Sc.QuotesListPerRFQ(QPR.Id);
                foreach (ServiceReference.Quotes Q in QL.Lista)
                {
                    DGV.Rows[i].Cells[1].Value = Q.VendorName;
                    foreach (RFQsDetails rd in rfQs011.RL.Lista[0].Details)
                    {
                        var qdl = Q.Details.Where(d => d.IdRFQDetail == rd.Id).ToList();
                        foreach (QuotesDetails qd in qdl)
                        {
                            DGV.Rows[i].Cells[2].Value = rd.NDC;
                            DGV.Rows[i].Cells[3].Value = rd.Description;
                            DGV.Rows[i].Cells[4].Value = rd.Qty.ToString("0.00");
                            DGV.Rows[i].Cells[5].Value = qd.AltNDC;
                            DGV.Rows[i].Cells[6].Value = qd.Manufacturer;
                            DGV.Rows[i].Cells[7].Value = qd.CountryOfOrigin;
                            DGV.Rows[i].Cells[8].Value = qd.AltQty.ToString("0.00");
                            DGV.Rows[i].Cells[9].Value = qd.SugestedUnitPrice.ToString("0.00");
                            DGV.Rows[i].Cells[10].Value = qd.CSPPrice.ToString("0.00");
                            DGV.Rows[i].Cells[11].Value = qd.Total.ToString("0.00");
                            DGV.Rows[i].Cells[12].Value = qd.Comments;
                            
                            if (Q.IncludedShipping) { DGV.Rows[i].Cells[13].Value = "Yes"; } else { DGV.Rows[i].Cells[13].Value = "No"; }
                            DGV.Rows[i].Cells[14].Value = Q.ShippingCost.ToString("0.00");
                            DGV.RowCount = DGV.RowCount + 1;
                            i++;
                        }
                    }
                    DGV.RowCount = DGV.RowCount + 1;
                    i++;
                }
            }catch (Exception e)
            {
                MessageBox.Show("Show per Vendor Error: " + e.Message);
            }

        }//End Show Vendor

        private void ShowItem(QuotesPerRFQs QPR)
        {
            int i = 3;
            DGV.Rows.Clear();
            DGV.Refresh();
            DGV.RowCount = 4;
            DGV.ColumnCount = 15;
            DGV.Rows[0].Cells[0].Value = "Solicitation:";
            DGV.Rows[0].Cells[1].Value = QPR.Solicitation;

            DGV.Rows[2].Cells[1].Value = "NDC";
            DGV.Rows[2].Cells[2].Value = "Vendor Name";
            DGV.Rows[2].Cells[3].Value = "Description";
            DGV.Rows[2].Cells[4].Value = "Qty";
            DGV.Rows[2].Cells[5].Value = "Alt NDC";
            DGV.Rows[2].Cells[6].Value = "Manufacturer";
            DGV.Rows[2].Cells[7].Value = "Country of Origin";
            DGV.Rows[2].Cells[8].Value = "Qty to be provided";
            DGV.Rows[2].Cells[9].Value = "Suggested Price";
            DGV.Rows[2].Cells[10].Value = "CSP Cost per Item";
            DGV.Rows[2].Cells[11].Value = "Total";
            DGV.Rows[2].Cells[12].Value = "Comments";
            DGV.Rows[2].Cells[13].Value = "Shipping Cost Included?";
            DGV.Rows[2].Cells[14].Value = "Shipping Cost";

            DGV.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DGV.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DGV.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DGV.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DGV.Columns[11].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DGV.Columns[14].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            ServiceClient Sc = new ServiceClient();
            try
            {
                QuotesList QL = Sc.QuotesListPerRFQ(QPR.Id);
                
                //foreach (ServiceReference.Quotes Q in QL.Lista)
                foreach (RFQsDetails rd in rfQs011.RL.Lista[0].Details)
                    {

                    string ant = "";
                    foreach (ServiceReference.Quotes Q in QL.Lista)
                    {
                        var qdl = Q.Details.Where(d => d.IdRFQDetail == rd.Id).ToList();
                        
                        foreach (QuotesDetails qd in qdl)
                        {
                            if (ant == "") { DGV.Rows[i].Cells[1].Value = rd.NDC; ant = rd.NDC; }

                            DGV.Rows[i].Cells[2].Value = Q.VendorName;
                            DGV.Rows[i].Cells[3].Value = rd.Description;
                            DGV.Rows[i].Cells[4].Value = rd.Qty.ToString("0.00");
                            DGV.Rows[i].Cells[5].Value = qd.AltNDC;
                            DGV.Rows[i].Cells[6].Value = qd.Manufacturer;
                            DGV.Rows[i].Cells[7].Value = qd.CountryOfOrigin;
                            DGV.Rows[i].Cells[8].Value = qd.AltQty.ToString("0.00");
                            DGV.Rows[i].Cells[9].Value = qd.SugestedUnitPrice.ToString("0.00");
                            DGV.Rows[i].Cells[10].Value = qd.CSPPrice.ToString("0.00");
                            DGV.Rows[i].Cells[11].Value = qd.Total.ToString("0.00");
                            DGV.Rows[i].Cells[12].Value = qd.Comments;

                            if (Q.IncludedShipping) { DGV.Rows[i].Cells[13].Value = "Yes"; } else { DGV.Rows[i].Cells[13].Value = "No"; }
                            DGV.Rows[i].Cells[14].Value = Q.ShippingCost.ToString("0.00");
                            DGV.RowCount = DGV.RowCount + 1;
                            i++;
                        }
                    }
                    if (ant != "")
                    {
                        DGV.RowCount = DGV.RowCount + 1;
                        i++;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Show per Vendor Error: " + e.Message);
            }

        }//End Show Item

        private void RdoBy_CheckedChanged(object sender, EventArgs e)
        {
            if (RdoVendor.Checked) ShowVendor(eQPR); else ShowItem(eQPR);
        }
    }
}
