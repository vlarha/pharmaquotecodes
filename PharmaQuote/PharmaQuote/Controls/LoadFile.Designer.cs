﻿namespace PharmaQuote.Controls
{
    partial class LoadFile
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DGV = new System.Windows.Forms.DataGridView();
            this.BtnSelectFile = new System.Windows.Forms.Button();
            this.LblFileName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.PgBar = new System.Windows.Forms.ProgressBar();
            this.CmbTemplate = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.BtnGenerator = new System.Windows.Forms.Button();
            this.LblTemplate = new System.Windows.Forms.Label();
            this.BtnCloseTemplate = new System.Windows.Forms.Button();
            this.BtnSN = new System.Windows.Forms.Button();
            this.LblSN = new System.Windows.Forms.Label();
            this.BtnPosted = new System.Windows.Forms.Button();
            this.LblPosted = new System.Windows.Forms.Label();
            this.BtnDue = new System.Windows.Forms.Button();
            this.LblDueDate = new System.Windows.Forms.Label();
            this.BtnHow = new System.Windows.Forms.Button();
            this.LblCustomer = new System.Windows.Forms.Label();
            this.BtnFr = new System.Windows.Forms.Button();
            this.LblFR = new System.Windows.Forms.Label();
            this.BtnNdc = new System.Windows.Forms.Button();
            this.LblNDC = new System.Windows.Forms.Label();
            this.BtnDescription = new System.Windows.Forms.Button();
            this.LblDesc = new System.Windows.Forms.Label();
            this.BtnQty = new System.Windows.Forms.Button();
            this.LblQty = new System.Windows.Forms.Label();
            this.LblCode = new System.Windows.Forms.Label();
            this.BtnClear = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.BtnSuggested = new System.Windows.Forms.Button();
            this.LblSuggested = new System.Windows.Forms.Label();
            this.BtnCSP = new System.Windows.Forms.Button();
            this.LblCSP = new System.Windows.Forms.Label();
            this.BtnTotal = new System.Windows.Forms.Button();
            this.LblTotal = new System.Windows.Forms.Label();
            this.TxtTemplate = new System.Windows.Forms.TextBox();
            this.PnTemplate = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.LblCountry = new System.Windows.Forms.Label();
            this.BtnCountry = new System.Windows.Forms.Button();
            this.LblManufacturer = new System.Windows.Forms.Label();
            this.BtnManufacturer = new System.Windows.Forms.Button();
            this.LblTemplateApplied = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.BtnSaveSN = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.BtnCheck = new System.Windows.Forms.Button();
            this.LblWarning = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DGV)).BeginInit();
            this.PnTemplate.SuspendLayout();
            this.SuspendLayout();
            // 
            // DGV
            // 
            this.DGV.AllowUserToAddRows = false;
            this.DGV.AllowUserToDeleteRows = false;
            this.DGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.DGV.Location = new System.Drawing.Point(3, 203);
            this.DGV.Name = "DGV";
            this.DGV.Size = new System.Drawing.Size(985, 524);
            this.DGV.TabIndex = 0;
            // 
            // BtnSelectFile
            // 
            this.BtnSelectFile.Location = new System.Drawing.Point(26, 13);
            this.BtnSelectFile.Name = "BtnSelectFile";
            this.BtnSelectFile.Size = new System.Drawing.Size(176, 24);
            this.BtnSelectFile.TabIndex = 2;
            this.BtnSelectFile.Text = "Select File";
            this.BtnSelectFile.UseVisualStyleBackColor = true;
            this.BtnSelectFile.Click += new System.EventHandler(this.BtnSelectFile_Click);
            // 
            // LblFileName
            // 
            this.LblFileName.AutoSize = true;
            this.LblFileName.Location = new System.Drawing.Point(3, 187);
            this.LblFileName.Name = "LblFileName";
            this.LblFileName.Size = new System.Drawing.Size(54, 13);
            this.LblFileName.TabIndex = 3;
            this.LblFileName.Text = "File Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "1";
            // 
            // PgBar
            // 
            this.PgBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PgBar.Location = new System.Drawing.Point(884, 189);
            this.PgBar.Name = "PgBar";
            this.PgBar.Size = new System.Drawing.Size(104, 13);
            this.PgBar.TabIndex = 5;
            this.PgBar.Visible = false;
            // 
            // CmbTemplate
            // 
            this.CmbTemplate.Enabled = false;
            this.CmbTemplate.FormattingEnabled = true;
            this.CmbTemplate.Items.AddRange(new object[] {
            "VA Consolidated",
            "VA COMP",
            "LA"});
            this.CmbTemplate.Location = new System.Drawing.Point(26, 53);
            this.CmbTemplate.Name = "CmbTemplate";
            this.CmbTemplate.Size = new System.Drawing.Size(176, 21);
            this.CmbTemplate.TabIndex = 3;
            this.CmbTemplate.Text = "Select template";
            this.CmbTemplate.SelectedIndexChanged += new System.EventHandler(this.CmbTemplate_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "2";
            // 
            // BtnGenerator
            // 
            this.BtnGenerator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnGenerator.Location = new System.Drawing.Point(876, 0);
            this.BtnGenerator.Name = "BtnGenerator";
            this.BtnGenerator.Size = new System.Drawing.Size(115, 25);
            this.BtnGenerator.TabIndex = 6;
            this.BtnGenerator.Text = "Template Generator";
            this.BtnGenerator.UseVisualStyleBackColor = true;
            this.BtnGenerator.Click += new System.EventHandler(this.BtnGenerator_Click);
            // 
            // LblTemplate
            // 
            this.LblTemplate.AutoSize = true;
            this.LblTemplate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTemplate.Location = new System.Drawing.Point(3, 6);
            this.LblTemplate.Name = "LblTemplate";
            this.LblTemplate.Size = new System.Drawing.Size(123, 13);
            this.LblTemplate.TabIndex = 0;
            this.LblTemplate.Text = "Template Generator:";
            // 
            // BtnCloseTemplate
            // 
            this.BtnCloseTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnCloseTemplate.Location = new System.Drawing.Point(567, 6);
            this.BtnCloseTemplate.Name = "BtnCloseTemplate";
            this.BtnCloseTemplate.Size = new System.Drawing.Size(19, 19);
            this.BtnCloseTemplate.TabIndex = 23;
            this.BtnCloseTemplate.Text = "X";
            this.BtnCloseTemplate.UseVisualStyleBackColor = true;
            this.BtnCloseTemplate.Click += new System.EventHandler(this.BtnCloseTemplate_Click);
            // 
            // BtnSN
            // 
            this.BtnSN.Location = new System.Drawing.Point(100, 29);
            this.BtnSN.Name = "BtnSN";
            this.BtnSN.Size = new System.Drawing.Size(41, 23);
            this.BtnSN.TabIndex = 8;
            this.BtnSN.Tag = "Solicitation #";
            this.BtnSN.Text = "< Set";
            this.BtnSN.UseVisualStyleBackColor = true;
            this.BtnSN.Click += new System.EventHandler(this.BtnTemplates_Click);
            // 
            // LblSN
            // 
            this.LblSN.AutoSize = true;
            this.LblSN.Location = new System.Drawing.Point(12, 34);
            this.LblSN.Name = "LblSN";
            this.LblSN.Size = new System.Drawing.Size(68, 13);
            this.LblSN.TabIndex = 3;
            this.LblSN.Text = "Solicitation #";
            // 
            // BtnPosted
            // 
            this.BtnPosted.Location = new System.Drawing.Point(100, 58);
            this.BtnPosted.Name = "BtnPosted";
            this.BtnPosted.Size = new System.Drawing.Size(41, 23);
            this.BtnPosted.TabIndex = 9;
            this.BtnPosted.Tag = "Posted Date";
            this.BtnPosted.Text = "< Set";
            this.BtnPosted.UseVisualStyleBackColor = true;
            this.BtnPosted.Click += new System.EventHandler(this.BtnTemplates_Click);
            // 
            // LblPosted
            // 
            this.LblPosted.AutoSize = true;
            this.LblPosted.Location = new System.Drawing.Point(12, 63);
            this.LblPosted.Name = "LblPosted";
            this.LblPosted.Size = new System.Drawing.Size(66, 13);
            this.LblPosted.TabIndex = 5;
            this.LblPosted.Text = "Posted Date";
            // 
            // BtnDue
            // 
            this.BtnDue.Location = new System.Drawing.Point(100, 87);
            this.BtnDue.Name = "BtnDue";
            this.BtnDue.Size = new System.Drawing.Size(41, 23);
            this.BtnDue.TabIndex = 10;
            this.BtnDue.Tag = "Due Date";
            this.BtnDue.Text = "< Set";
            this.BtnDue.UseVisualStyleBackColor = true;
            this.BtnDue.Click += new System.EventHandler(this.BtnTemplates_Click);
            // 
            // LblDueDate
            // 
            this.LblDueDate.AutoSize = true;
            this.LblDueDate.Location = new System.Drawing.Point(12, 92);
            this.LblDueDate.Name = "LblDueDate";
            this.LblDueDate.Size = new System.Drawing.Size(53, 13);
            this.LblDueDate.TabIndex = 7;
            this.LblDueDate.Text = "Due Date";
            // 
            // BtnHow
            // 
            this.BtnHow.Location = new System.Drawing.Point(100, 116);
            this.BtnHow.Name = "BtnHow";
            this.BtnHow.Size = new System.Drawing.Size(41, 23);
            this.BtnHow.TabIndex = 11;
            this.BtnHow.Tag = "Customer";
            this.BtnHow.Text = "< Set";
            this.BtnHow.UseVisualStyleBackColor = true;
            this.BtnHow.Click += new System.EventHandler(this.BtnTemplates_Click);
            // 
            // LblCustomer
            // 
            this.LblCustomer.AutoSize = true;
            this.LblCustomer.Location = new System.Drawing.Point(12, 121);
            this.LblCustomer.Name = "LblCustomer";
            this.LblCustomer.Size = new System.Drawing.Size(51, 13);
            this.LblCustomer.TabIndex = 9;
            this.LblCustomer.Text = "Customer";
            // 
            // BtnFr
            // 
            this.BtnFr.Location = new System.Drawing.Point(244, 29);
            this.BtnFr.Name = "BtnFr";
            this.BtnFr.Size = new System.Drawing.Size(41, 23);
            this.BtnFr.TabIndex = 12;
            this.BtnFr.Tag = "First Row Details";
            this.BtnFr.Text = "< Set";
            this.BtnFr.UseVisualStyleBackColor = true;
            this.BtnFr.Click += new System.EventHandler(this.BtnTemplates_Click);
            // 
            // LblFR
            // 
            this.LblFR.AutoSize = true;
            this.LblFR.Location = new System.Drawing.Point(156, 34);
            this.LblFR.Name = "LblFR";
            this.LblFR.Size = new System.Drawing.Size(86, 13);
            this.LblFR.TabIndex = 11;
            this.LblFR.Text = "First Row Details";
            // 
            // BtnNdc
            // 
            this.BtnNdc.Location = new System.Drawing.Point(244, 58);
            this.BtnNdc.Name = "BtnNdc";
            this.BtnNdc.Size = new System.Drawing.Size(41, 23);
            this.BtnNdc.TabIndex = 13;
            this.BtnNdc.Tag = "NDC Col";
            this.BtnNdc.Text = "< Set";
            this.BtnNdc.UseVisualStyleBackColor = true;
            this.BtnNdc.Click += new System.EventHandler(this.BtnTemplates_Click);
            // 
            // LblNDC
            // 
            this.LblNDC.AutoSize = true;
            this.LblNDC.Location = new System.Drawing.Point(156, 63);
            this.LblNDC.Name = "LblNDC";
            this.LblNDC.Size = new System.Drawing.Size(48, 13);
            this.LblNDC.TabIndex = 13;
            this.LblNDC.Text = "NDC Col";
            // 
            // BtnDescription
            // 
            this.BtnDescription.Location = new System.Drawing.Point(244, 87);
            this.BtnDescription.Name = "BtnDescription";
            this.BtnDescription.Size = new System.Drawing.Size(41, 23);
            this.BtnDescription.TabIndex = 14;
            this.BtnDescription.Tag = "Description Col";
            this.BtnDescription.Text = "< Set";
            this.BtnDescription.UseVisualStyleBackColor = true;
            this.BtnDescription.Click += new System.EventHandler(this.BtnTemplates_Click);
            // 
            // LblDesc
            // 
            this.LblDesc.AutoSize = true;
            this.LblDesc.Location = new System.Drawing.Point(156, 92);
            this.LblDesc.Name = "LblDesc";
            this.LblDesc.Size = new System.Drawing.Size(78, 13);
            this.LblDesc.TabIndex = 15;
            this.LblDesc.Text = "Description Col";
            // 
            // BtnQty
            // 
            this.BtnQty.Location = new System.Drawing.Point(244, 116);
            this.BtnQty.Name = "BtnQty";
            this.BtnQty.Size = new System.Drawing.Size(41, 23);
            this.BtnQty.TabIndex = 15;
            this.BtnQty.Tag = "Quantity Col";
            this.BtnQty.Text = "< Set";
            this.BtnQty.UseVisualStyleBackColor = true;
            this.BtnQty.Click += new System.EventHandler(this.BtnTemplates_Click);
            // 
            // LblQty
            // 
            this.LblQty.AutoSize = true;
            this.LblQty.Location = new System.Drawing.Point(156, 121);
            this.LblQty.Name = "LblQty";
            this.LblQty.Size = new System.Drawing.Size(64, 13);
            this.LblQty.TabIndex = 17;
            this.LblQty.Text = "Quantity Col";
            // 
            // LblCode
            // 
            this.LblCode.AutoSize = true;
            this.LblCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCode.Location = new System.Drawing.Point(132, 6);
            this.LblCode.Name = "LblCode";
            this.LblCode.Size = new System.Drawing.Size(0, 13);
            this.LblCode.TabIndex = 18;
            // 
            // BtnClear
            // 
            this.BtnClear.Location = new System.Drawing.Point(498, 157);
            this.BtnClear.Name = "BtnClear";
            this.BtnClear.Size = new System.Drawing.Size(41, 23);
            this.BtnClear.TabIndex = 21;
            this.BtnClear.Text = "Clear";
            this.BtnClear.UseVisualStyleBackColor = true;
            this.BtnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.Enabled = false;
            this.BtnSave.Location = new System.Drawing.Point(545, 157);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(41, 23);
            this.BtnSave.TabIndex = 22;
            this.BtnSave.Text = "Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BtnSuggested
            // 
            this.BtnSuggested.Location = new System.Drawing.Point(392, 29);
            this.BtnSuggested.Name = "BtnSuggested";
            this.BtnSuggested.Size = new System.Drawing.Size(41, 23);
            this.BtnSuggested.TabIndex = 16;
            this.BtnSuggested.Tag = "Suggested Price";
            this.BtnSuggested.Text = "< Set";
            this.BtnSuggested.UseVisualStyleBackColor = true;
            this.BtnSuggested.Click += new System.EventHandler(this.BtnTemplates_Click);
            // 
            // LblSuggested
            // 
            this.LblSuggested.AutoSize = true;
            this.LblSuggested.Location = new System.Drawing.Point(304, 34);
            this.LblSuggested.Name = "LblSuggested";
            this.LblSuggested.Size = new System.Drawing.Size(85, 13);
            this.LblSuggested.TabIndex = 22;
            this.LblSuggested.Text = "Suggested Price";
            // 
            // BtnCSP
            // 
            this.BtnCSP.Location = new System.Drawing.Point(392, 58);
            this.BtnCSP.Name = "BtnCSP";
            this.BtnCSP.Size = new System.Drawing.Size(41, 23);
            this.BtnCSP.TabIndex = 17;
            this.BtnCSP.Tag = "CSP Price";
            this.BtnCSP.Text = "< Set";
            this.BtnCSP.UseVisualStyleBackColor = true;
            this.BtnCSP.Click += new System.EventHandler(this.BtnTemplates_Click);
            // 
            // LblCSP
            // 
            this.LblCSP.AutoSize = true;
            this.LblCSP.Location = new System.Drawing.Point(304, 63);
            this.LblCSP.Name = "LblCSP";
            this.LblCSP.Size = new System.Drawing.Size(55, 13);
            this.LblCSP.TabIndex = 24;
            this.LblCSP.Text = "CSP Price";
            // 
            // BtnTotal
            // 
            this.BtnTotal.Location = new System.Drawing.Point(392, 87);
            this.BtnTotal.Name = "BtnTotal";
            this.BtnTotal.Size = new System.Drawing.Size(41, 23);
            this.BtnTotal.TabIndex = 18;
            this.BtnTotal.Tag = "Total";
            this.BtnTotal.Text = "< Set";
            this.BtnTotal.UseVisualStyleBackColor = true;
            this.BtnTotal.Click += new System.EventHandler(this.BtnTemplates_Click);
            // 
            // LblTotal
            // 
            this.LblTotal.AutoSize = true;
            this.LblTotal.Location = new System.Drawing.Point(304, 92);
            this.LblTotal.Name = "LblTotal";
            this.LblTotal.Size = new System.Drawing.Size(31, 13);
            this.LblTotal.TabIndex = 26;
            this.LblTotal.Text = "Total";
            // 
            // TxtTemplate
            // 
            this.TxtTemplate.Location = new System.Drawing.Point(15, 159);
            this.TxtTemplate.Name = "TxtTemplate";
            this.TxtTemplate.Size = new System.Drawing.Size(150, 20);
            this.TxtTemplate.TabIndex = 7;
            this.TxtTemplate.Tag = "0";
            this.TxtTemplate.TextChanged += new System.EventHandler(this.TxtTemplate_TextChanged);
            // 
            // PnTemplate
            // 
            this.PnTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PnTemplate.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.PnTemplate.Controls.Add(this.label3);
            this.PnTemplate.Controls.Add(this.LblCountry);
            this.PnTemplate.Controls.Add(this.BtnCountry);
            this.PnTemplate.Controls.Add(this.LblManufacturer);
            this.PnTemplate.Controls.Add(this.BtnManufacturer);
            this.PnTemplate.Controls.Add(this.TxtTemplate);
            this.PnTemplate.Controls.Add(this.LblTotal);
            this.PnTemplate.Controls.Add(this.BtnTotal);
            this.PnTemplate.Controls.Add(this.LblCSP);
            this.PnTemplate.Controls.Add(this.BtnCSP);
            this.PnTemplate.Controls.Add(this.LblSuggested);
            this.PnTemplate.Controls.Add(this.BtnSuggested);
            this.PnTemplate.Controls.Add(this.BtnSave);
            this.PnTemplate.Controls.Add(this.BtnClear);
            this.PnTemplate.Controls.Add(this.LblCode);
            this.PnTemplate.Controls.Add(this.LblQty);
            this.PnTemplate.Controls.Add(this.BtnQty);
            this.PnTemplate.Controls.Add(this.LblDesc);
            this.PnTemplate.Controls.Add(this.BtnDescription);
            this.PnTemplate.Controls.Add(this.LblNDC);
            this.PnTemplate.Controls.Add(this.BtnNdc);
            this.PnTemplate.Controls.Add(this.LblFR);
            this.PnTemplate.Controls.Add(this.BtnFr);
            this.PnTemplate.Controls.Add(this.LblCustomer);
            this.PnTemplate.Controls.Add(this.BtnHow);
            this.PnTemplate.Controls.Add(this.LblDueDate);
            this.PnTemplate.Controls.Add(this.BtnDue);
            this.PnTemplate.Controls.Add(this.LblPosted);
            this.PnTemplate.Controls.Add(this.BtnPosted);
            this.PnTemplate.Controls.Add(this.LblSN);
            this.PnTemplate.Controls.Add(this.BtnSN);
            this.PnTemplate.Controls.Add(this.BtnCloseTemplate);
            this.PnTemplate.Controls.Add(this.LblTemplate);
            this.PnTemplate.Location = new System.Drawing.Point(401, -2);
            this.PnTemplate.Name = "PnTemplate";
            this.PnTemplate.Size = new System.Drawing.Size(589, 183);
            this.PnTemplate.TabIndex = 10;
            this.PnTemplate.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 144);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "Template Name:";
            // 
            // LblCountry
            // 
            this.LblCountry.AutoSize = true;
            this.LblCountry.Location = new System.Drawing.Point(457, 63);
            this.LblCountry.Name = "LblCountry";
            this.LblCountry.Size = new System.Drawing.Size(73, 13);
            this.LblCountry.TabIndex = 31;
            this.LblCountry.Text = "Country Origin";
            // 
            // BtnCountry
            // 
            this.BtnCountry.Location = new System.Drawing.Point(545, 58);
            this.BtnCountry.Name = "BtnCountry";
            this.BtnCountry.Size = new System.Drawing.Size(41, 23);
            this.BtnCountry.TabIndex = 20;
            this.BtnCountry.Tag = "Country Origin";
            this.BtnCountry.Text = "< Set";
            this.BtnCountry.UseVisualStyleBackColor = true;
            this.BtnCountry.Click += new System.EventHandler(this.BtnTemplates_Click);
            // 
            // LblManufacturer
            // 
            this.LblManufacturer.AutoSize = true;
            this.LblManufacturer.Location = new System.Drawing.Point(457, 34);
            this.LblManufacturer.Name = "LblManufacturer";
            this.LblManufacturer.Size = new System.Drawing.Size(70, 13);
            this.LblManufacturer.TabIndex = 29;
            this.LblManufacturer.Text = "Manufacturer";
            // 
            // BtnManufacturer
            // 
            this.BtnManufacturer.Location = new System.Drawing.Point(545, 29);
            this.BtnManufacturer.Name = "BtnManufacturer";
            this.BtnManufacturer.Size = new System.Drawing.Size(41, 23);
            this.BtnManufacturer.TabIndex = 19;
            this.BtnManufacturer.Tag = "Manufacturer";
            this.BtnManufacturer.Text = "< Set";
            this.BtnManufacturer.UseVisualStyleBackColor = true;
            this.BtnManufacturer.Click += new System.EventHandler(this.BtnTemplates_Click);
            // 
            // LblTemplateApplied
            // 
            this.LblTemplateApplied.AutoSize = true;
            this.LblTemplateApplied.Location = new System.Drawing.Point(4, 170);
            this.LblTemplateApplied.Name = "LblTemplateApplied";
            this.LblTemplateApplied.Size = new System.Drawing.Size(0, 13);
            this.LblTemplateApplied.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 20);
            this.label4.TabIndex = 13;
            this.label4.Text = "4";
            // 
            // BtnSaveSN
            // 
            this.BtnSaveSN.Enabled = false;
            this.BtnSaveSN.Location = new System.Drawing.Point(26, 133);
            this.BtnSaveSN.Name = "BtnSaveSN";
            this.BtnSaveSN.Size = new System.Drawing.Size(176, 24);
            this.BtnSaveSN.TabIndex = 5;
            this.BtnSaveSN.Text = "Save Solicitation";
            this.BtnSaveSN.UseVisualStyleBackColor = true;
            this.BtnSaveSN.Click += new System.EventHandler(this.BtnSaveSN_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(18, 20);
            this.label5.TabIndex = 15;
            this.label5.Text = "3";
            // 
            // BtnCheck
            // 
            this.BtnCheck.Enabled = false;
            this.BtnCheck.Location = new System.Drawing.Point(26, 92);
            this.BtnCheck.Name = "BtnCheck";
            this.BtnCheck.Size = new System.Drawing.Size(176, 24);
            this.BtnCheck.TabIndex = 4;
            this.BtnCheck.Text = "Check";
            this.BtnCheck.UseVisualStyleBackColor = true;
            this.BtnCheck.Click += new System.EventHandler(this.BtnCheck_Click);
            // 
            // LblWarning
            // 
            this.LblWarning.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LblWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblWarning.ForeColor = System.Drawing.Color.Red;
            this.LblWarning.Location = new System.Drawing.Point(615, 184);
            this.LblWarning.Name = "LblWarning";
            this.LblWarning.Size = new System.Drawing.Size(372, 16);
            this.LblWarning.TabIndex = 16;
            this.LblWarning.Text = "Please Check the fileds in Red";
            this.LblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.LblWarning.Visible = false;
            // 
            // LoadFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.LblWarning);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.BtnCheck);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.BtnSaveSN);
            this.Controls.Add(this.LblTemplateApplied);
            this.Controls.Add(this.PnTemplate);
            this.Controls.Add(this.BtnGenerator);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CmbTemplate);
            this.Controls.Add(this.PgBar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LblFileName);
            this.Controls.Add(this.BtnSelectFile);
            this.Controls.Add(this.DGV);
            this.Name = "LoadFile";
            this.Size = new System.Drawing.Size(991, 738);
            ((System.ComponentModel.ISupportInitialize)(this.DGV)).EndInit();
            this.PnTemplate.ResumeLayout(false);
            this.PnTemplate.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DGV;
        private System.Windows.Forms.Button BtnSelectFile;
        private System.Windows.Forms.Label LblFileName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar PgBar;
        private System.Windows.Forms.ComboBox CmbTemplate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BtnGenerator;
        private System.Windows.Forms.Label LblTemplate;
        private System.Windows.Forms.Button BtnCloseTemplate;
        private System.Windows.Forms.Button BtnSN;
        private System.Windows.Forms.Label LblSN;
        private System.Windows.Forms.Button BtnPosted;
        private System.Windows.Forms.Label LblPosted;
        private System.Windows.Forms.Button BtnDue;
        private System.Windows.Forms.Label LblDueDate;
        private System.Windows.Forms.Button BtnHow;
        private System.Windows.Forms.Label LblCustomer;
        private System.Windows.Forms.Button BtnFr;
        private System.Windows.Forms.Label LblFR;
        private System.Windows.Forms.Button BtnNdc;
        private System.Windows.Forms.Label LblNDC;
        private System.Windows.Forms.Button BtnDescription;
        private System.Windows.Forms.Label LblDesc;
        private System.Windows.Forms.Button BtnQty;
        private System.Windows.Forms.Label LblQty;
        private System.Windows.Forms.Label LblCode;
        private System.Windows.Forms.Button BtnClear;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Button BtnSuggested;
        private System.Windows.Forms.Label LblSuggested;
        private System.Windows.Forms.Button BtnCSP;
        private System.Windows.Forms.Label LblCSP;
        private System.Windows.Forms.Button BtnTotal;
        private System.Windows.Forms.Label LblTotal;
        private System.Windows.Forms.TextBox TxtTemplate;
        private System.Windows.Forms.Panel PnTemplate;
        private System.Windows.Forms.Label LblManufacturer;
        private System.Windows.Forms.Button BtnManufacturer;
        private System.Windows.Forms.Label LblCountry;
        private System.Windows.Forms.Button BtnCountry;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LblTemplateApplied;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button BtnSaveSN;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button BtnCheck;
        private System.Windows.Forms.Label LblWarning;
    }
}
