﻿using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing;
using PharmaQuote.ServiceReference;

namespace PharmaQuote.Controls
{
    public partial class LoadFile : UserControl
    {
        TemplatesList Lista;
        public LoadFile()
        {
            InitializeComponent();
            //FillTemplates();            
        }

        private void FillTemplates()
        {
            CmbTemplate.Items.Clear();
            try
            {
                ServiceClient SC = new ServiceClient();
                Lista = SC.Templates_List();                
                foreach (Templates T in Lista.List) CmbTemplate.Items.Add(T.Name);
            }
            catch
            {
                MessageBox.Show("Error: Trying to reach the services");
            }
            CmbTemplate.Text="Select Template";

        }//End FillTemplates

        private void BtnSelectFile_Click(object sender, EventArgs e)
        {
            string fname = "";
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "Excel File Dialog";
            fdlg.InitialDirectory = @"c:\";
            fdlg.Filter = "All files (*.*)|*.*|All files (*.*)|*.*";
            fdlg.FilterIndex = 2;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                fname = fdlg.FileName;
                LblFileName.Text = fdlg.FileName;
                LblTemplateApplied.Text = "";
                PgBar.Value = 0;
                BtnSelectFile.Enabled = false;
                BtnSaveSN.Enabled = false;
                BtnCheck.Enabled = false;
                CmbTemplate.Enabled = false;
                LblWarning.Visible = false;
                CmbTemplate.Text = "Select Template";
                DGV.Rows.Clear();
                DGV.Refresh();

                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(fname);
                Microsoft.Office.Interop.Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
                Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;

                int rowCount = xlRange.Rows.Count;
                int colCount = xlRange.Columns.Count;

                DGV.ColumnCount = colCount;
                DGV.RowCount = rowCount;
                PgBar.Maximum = colCount * rowCount;
                PgBar.Visible = true;
                for (int i = 1; i <= rowCount; i++)
                {
                    for (int j = 1; j <= colCount; j++)
                    {
                        PgBar.Value = PgBar.Value + 1;
                        //write the value to the Grid  
                        Application.DoEvents();
                        if (xlRange.Cells[i, j] != null && xlRange.Cells[i, j].Value2 != null)
                        {
                            DGV.Rows[i - 1].Cells[j - 1].Value = xlRange.Cells[i, j].Value2.ToString();
                        }
                        // Console.Write(xlRange.Cells[i, j].Value2.ToString() + "\t");  
                    }
                }

                //cleanup  
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //rule of thumb for releasing com objects:  
                //  never use two dots, all COM objects must be referenced and released individually  
                //  ex: [somthing].[something].[something] is bad  

                //release com objects to fully kill excel process from running in the background  
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);

                //close and release  
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);

                //quit and release  
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                PgBar.Visible = false;
                BtnSelectFile.Enabled = true;
                FillTemplates();
                CmbTemplate.Enabled = true;                
            }
            else { }
        }//End BtnSelectFile

        //Remove the template from the Grid
        private void TemplateClear()
        {
            DataGridViewCellStyle styleB = new DataGridViewCellStyle();
            styleB.BackColor = Color.White;
            styleB.ForeColor = Color.Black;


            for (int i = 0; i <= DGV.RowCount - 1; i++)
            {
                for (int j = 0; j <= DGV.ColumnCount - 1; j++)
                {
                    DGV.Rows[i].Cells[j].Style = styleB;
                }
            }
        }//End Template Clear

        //Show the selection by template in the grid
        private void TemplateApply(string template)
        {
            string[] keys = template.Split(';');
            TemplateClear();

            //Style Remark cells
            DataGridViewCellStyle style = new DataGridViewCellStyle();
            style.BackColor = Color.Black;
            style.ForeColor = Color.White;

            //Solicitation Number  
            string[] SN = keys[0].Split(',');
            DGV.Rows[Int32.Parse(SN[1])].Cells[Int32.Parse(SN[0])].Style = style;

            //Posted Date
            string[] Pd = keys[1].Split(',');
            DGV.Rows[Int32.Parse(Pd[1])].Cells[Int32.Parse(Pd[0])].Style = style;

            //Due Date
            string[] Dd = keys[2].Split(',');
            DGV.Rows[Int32.Parse(Dd[1])].Cells[Int32.Parse(Dd[0])].Style = style;

            //Client Name
            string[] CN = keys[3].Split(',');
            DGV.Rows[Int32.Parse(CN[1])].Cells[Int32.Parse(CN[0])].Style = style;

            //Detailsss
            //First Record Row
            int FR = Int32.Parse(keys[4]);

            //NDC Col
            int ND = Int32.Parse(keys[5]);

            //Description
            int Ds = Int32.Parse(keys[6]);

            //Quantity
            int Qt = Int32.Parse(keys[7]);

            //Suggested Price
            int Su = Int32.Parse(keys[8]);

            //CSP Price
            int CS = Int32.Parse(keys[9]);

            //Total Price
            int To = Int32.Parse(keys[10]);

            //Manufacturer
            int Ma = Int32.Parse(keys[11]);

            //Country
            int Co = Int32.Parse(keys[12]);


            int n = 0;
            try
            {
                if (DGV.Rows[FR].Cells[Qt].Value == null) n = 1;
                while (n == 0)
                {
                    //NDC
                    DGV.Rows[FR].Cells[ND].Style = style;
                    //Description
                    DGV.Rows[FR].Cells[Ds].Style = style;
                    //Qty
                    DGV.Rows[FR].Cells[Qt].Style = style;
                    //Suggested
                    DGV.Rows[FR].Cells[Su].Style = style;
                    //CSP Price
                    DGV.Rows[FR].Cells[CS].Style = style;
                    //Total Price
                    DGV.Rows[FR].Cells[To].Style = style;
                    //Manufacurer
                    DGV.Rows[FR].Cells[Ma].Style = style;
                    //Country
                    DGV.Rows[FR].Cells[Co].Style = style;
                    FR = FR + 1;

                    //Exit control
                    if (DGV.Rows[FR].Cells[Qt].Value == null) n = 1;
                }
            }
            catch (Exception ex) { string resp = ex.ToString(); }
            LblTemplateApplied.Text = template;
            BtnCheck.Enabled = true;
        }//End TemplateApply

        //Change Template
        private void CmbTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            //         S#  Pos Due How First detail,NDC col,Description col, Qty Col, Suggested Price Col, CSP Price Col, Total Col
            //Template x,x:x,x;x,x;x,x;x;x;x;x
            string Template="";
            LblWarning.Visible = false;
            BtnCheck.Enabled = false;
            BtnSaveSN.Enabled = false;

            foreach (Templates t in Lista.List)
            {
                if (t.Name == CmbTemplate.Text) Template = t.Template;
            }
            TemplateApply(Template);            
        }//End CmbTemplate

        private void BtnGenerator_Click(object sender, EventArgs e)
        {
            PnTemplate.Visible = true;
        }//End BtnGenerator

        private void BtnCloseTemplate_Click(object sender, EventArgs e)
        {
            PnTemplate.Visible = false;
            BtnClear_Click(sender, e);
        }//End BtnClose Template

        //Used for all Buttons per each field
        private void BtnTemplates_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (DGV.Rows.Count > 0)
            {
                foreach (object obj in PnTemplate.Controls)
                {
                    if (obj.GetType().ToString() == "System.Windows.Forms.Label")
                    {
                        Label l = (Label)obj;
                        if (l.Text == btn.Tag.ToString())
                        {
                            int row = DGV.CurrentCell.RowIndex;
                            int col = DGV.CurrentCell.ColumnIndex;
                            switch (l.Text)
                            {
                                case "First Row Details":
                                    l.Tag = "" + row;
                                    break;
                                case "NDC Col":
                                    l.Tag = "" + col;
                                    break;
                                case "Description Col":
                                    l.Tag = "" + col;
                                    break;
                                case "Quantity Col":
                                    l.Tag = "" + col;
                                    break;
                                case "Suggested Price":
                                    l.Tag = "" + col;
                                    break;
                                case "CSP Price":
                                    l.Tag = "" + col;
                                    break;
                                case "Total":
                                    l.Tag = "" + col;
                                    break;
                                case "Manufacturer":
                                    l.Tag = "" + col;
                                    break;
                                case "Country Origin":
                                    l.Tag = "" + col;
                                    break;
                                default:
                                    l.Tag = "" + col + "," + row;
                                    break;
                            }
                            //l.Tag = ""+ col + "," + row;
                            l.ForeColor = Color.Blue;
                        }
                    }
                }
            }//End rows.count
            TemGen();
        }//End BtnTemplate

        //Verify the template formation
        private void TemGen()
        {
            int sum = 0;
            LblCode.Text = "";
            if (LblSN.Tag != null) { LblCode.Text = LblSN.Tag.ToString() + ";"; sum++; }
            if (LblPosted.Tag != null) { LblCode.Text = LblCode.Text + LblPosted.Tag.ToString() + ";"; sum++; }
            if (LblDueDate.Tag != null) { LblCode.Text = LblCode.Text + LblDueDate.Tag.ToString() + ";"; sum++; }
            if (LblCustomer.Tag != null) { LblCode.Text = LblCode.Text + LblCustomer.Tag.ToString() + ";"; sum++; }
            if (LblFR.Tag != null) { LblCode.Text = LblCode.Text + LblFR.Tag.ToString() + ";"; sum++; }
            if (LblNDC.Tag != null) { LblCode.Text = LblCode.Text + LblNDC.Tag.ToString() + ";"; sum++; }
            if (LblDesc.Tag != null) { LblCode.Text = LblCode.Text + LblDesc.Tag.ToString() + ";"; sum++; }
            if (LblQty.Tag != null) { LblCode.Text = LblCode.Text + LblQty.Tag.ToString() + ";"; sum++; }
            if (LblSuggested.Tag != null) { LblCode.Text = LblCode.Text + LblSuggested.Tag.ToString() + ";"; sum++; }
            if (LblCSP.Tag != null) { LblCode.Text = LblCode.Text + LblCSP.Tag.ToString() + ";"; sum++; }
            if (LblTotal.Tag != null) { LblCode.Text = LblCode.Text + LblTotal.Tag.ToString() + ";"; sum++; }
            if (LblManufacturer.Tag != null) { LblCode.Text = LblCode.Text + LblManufacturer.Tag.ToString() + ";"; sum++; }
            if (LblCountry.Tag != null) { LblCode.Text = LblCode.Text + LblCountry.Tag.ToString(); sum++; }
            TxtTemplate.Tag = "" + sum;
            if (TxtTemplate.Tag.ToString() == "13" && TxtTemplate.Text != "") BtnSave.Enabled = true; else BtnSave.Enabled = false;
        }//End TemGen

        //Clear all inforation for template generation
        private void BtnClear_Click(object sender, EventArgs e)
        {            
            foreach (object obj in PnTemplate.Controls)
            {
                if (obj.GetType().ToString() == "System.Windows.Forms.Label")
                {
                    Label l = (Label)obj;
                    l.Tag = null;
                    l.ForeColor = Color.Black;                    
                }
            }
            TxtTemplate.Text = "";
            TemGen();
        }//End BtnTemplate

        //Save the template
        private void BtnSave_Click(object sender, EventArgs e)
        {
            Templates t = new Templates();
            t.Name = TxtTemplate.Text;
            t.Template = LblCode.Text;
            try
            {
                ServiceClient SC = new ServiceClient();                
                string Resp = SC.AddTemplate(t);
                MessageBox.Show(Resp);
                FillTemplates();
            }catch(Exception ex)
            {
                MessageBox.Show("Error Trying reach the services");
            }

            TemplateApply(LblCode.Text);
        }

        //Template name change
        private void TxtTemplate_TextChanged(object sender, EventArgs e)
        {
            if (TxtTemplate.Tag.ToString() == "13" && TxtTemplate.Text != "") BtnSave.Enabled = true; else BtnSave.Enabled = false;
        }


        //Load details and general information into a class
        private void BtnSaveSN_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            //First CHeck All Fields
            string template = LblTemplateApplied.Text;
            string[] keys = template.Split(';');            
            RFQs RFQ = new RFQs();

            //Solicitation Number  
            string[] SN = keys[0].Split(',');
            RFQ.Solicitation = DGV.Rows[Int32.Parse(SN[1])].Cells[Int32.Parse(SN[0])].Value.ToString();

            //Posted Date
            string[] Pd = keys[1].Split(',');
            RFQ.Posted = DateTime.Parse(DGV.Rows[Int32.Parse(Pd[1])].Cells[Int32.Parse(Pd[0])].Value.ToString());
            
            //Due Date
            string[] Dd = keys[2].Split(',');
            RFQ.Due = DateTime.Parse(DGV.Rows[Int32.Parse(Dd[1])].Cells[Int32.Parse(Dd[0])].Value.ToString());

            //Client Name
            string[] CN = keys[3].Split(',');
            RFQ.Customer = DGV.Rows[Int32.Parse(CN[1])].Cells[Int32.Parse(CN[0])].Value.ToString();

            //Detailsss
            //First Record Row
            int FR = Int32.Parse(keys[4]);

            //NDC Col
            int ND = Int32.Parse(keys[5]);

            //Description
            int Ds = Int32.Parse(keys[6]);

            //Quantity
            int Qt = Int32.Parse(keys[7]);

            //Suggested Price
            int Su = Int32.Parse(keys[8]);

            //CSP Price
            int CS = Int32.Parse(keys[9]);

            //Total Price
            int To = Int32.Parse(keys[10]);

            //Manufacturer
            int Ma = Int32.Parse(keys[11]);

            //Country
            int Co = Int32.Parse(keys[12]);
            
            int n = 0;
            RFQ.Details = new System.Collections.Generic.List<RFQsDetails>();
            if (DGV.Rows[FR].Cells[Qt].Value == null) n = 1;
            while (n == 0)
            {
                RFQsDetails Deta = new RFQsDetails();
                //NDC
                Deta.NDC = DGV.Rows[FR].Cells[ND].Value.ToString();
                //Description
                Deta.Description = DGV.Rows[FR].Cells[Ds].Value.ToString();
                //Qty
                Deta.Qty = Int32.Parse(DGV.Rows[FR].Cells[Qt].Value.ToString());                
                FR = FR + 1;
                RFQ.Details.Add(Deta);
                //Exit control
                try
                {
                    if (DGV.Rows[FR].Cells[Qt].Value == null) n = 1;
                }
                catch { n = 1; }
            }
            RFQ.IdUser = 1;
            RFQ.Template = LblTemplateApplied.Text;
            RFQ.FileName = LblFileName.Text;
            RFQ.Created = DateTime.Now;
            RFQ.Status = "Created";

            try
            {
                ServiceClient Sc = new ServiceClient();
                long i = Sc.CheckRFQ(RFQ.Solicitation);
                if (i < -1) { MessageBox.Show("Problems to communicate with the server; Try again."); }
                if (i > 0) { MessageBox.Show("Currently there is an Solicitation with the same number and is active"); }
                if (i == 0)
                {

                    string resp = Sc.SaveRFQs(RFQ);
                    if (resp == "Ok")
                    {
                        MessageBox.Show("The file has been loaded correctly");
                        //DGV.Enabled = false;
                    }
                    else
                    {
                        MessageBox.Show(resp);
                    }
                }//End if == 0
            }
            catch (Exception ex)
            {
                MessageBox.Show("Communication error, please try again");
            }
            Cursor.Current = Cursors.Default;
        } //End BtnSave

        //Check and validate the fileds in the file
        private void BtnCheck_Click(object sender, EventArgs e)
        {
            TemplateClear();
            LblWarning.Text = "Please Check the fileds in Red.";
            LblWarning.Visible = false;
            BtnSaveSN.Enabled = false;
            int error = 0;

            DataGridViewCellStyle style = new DataGridViewCellStyle();
            style.BackColor = Color.Red;
            style.ForeColor = Color.Black;

            DataGridViewCellStyle styleGood = new DataGridViewCellStyle();
            styleGood.BackColor = Color.Blue;
            styleGood.ForeColor = Color.Black;

            //First CHeck All Fields
            string template = LblTemplateApplied.Text;            

            string[] keys = template.Split(';');

            //Solicitation Number  
            string[] SN = keys[0].Split(',');
            if (DGV.Rows[Int32.Parse(SN[1])].Cells[Int32.Parse(SN[0])].Value == null) { DGV.Rows[Int32.Parse(SN[1])].Cells[Int32.Parse(SN[0])].Style = style; error = 1; } else DGV.Rows[Int32.Parse(SN[1])].Cells[Int32.Parse(SN[0])].Style = styleGood;

            //Posted Date
            string[] Pd = keys[1].Split(',');            
            if (DGV.Rows[Int32.Parse(Pd[1])].Cells[Int32.Parse(Pd[0])].Value == null)
            {
                DGV.Rows[Int32.Parse(Pd[1])].Cells[Int32.Parse(Pd[0])].Style = style;
            }
            else
            {

                try
                {
                    DateTime Dt;
                    Dt = DateTime.Parse(DGV.Rows[Int32.Parse(Pd[1])].Cells[Int32.Parse(Pd[0])].Value.ToString());
                    DGV.Rows[Int32.Parse(Pd[1])].Cells[Int32.Parse(Pd[0])].Style = styleGood;
                }
                catch(Exception ex)
                {
                    try
                    {
                        DateTime Dt = new DateTime(1899, 12, 31).AddDays(Int64.Parse(DGV.Rows[Int32.Parse(Pd[1])].Cells[Int32.Parse(Pd[0])].Value.ToString()));
                        DGV.Rows[Int32.Parse(Pd[1])].Cells[Int32.Parse(Pd[0])].Value = Dt.ToString("MM/dd/yyyy");
                        DGV.Rows[Int32.Parse(Pd[1])].Cells[Int32.Parse(Pd[0])].Style = styleGood;
                    }
                    catch(Exception Ex01)
                    {
                        DGV.Rows[Int32.Parse(Pd[1])].Cells[Int32.Parse(Pd[0])].Style = style;
                        error = 1;
                    }
                    
                }
            }

            //Due Date
            string[] Dd = keys[2].Split(',');
            if (DGV.Rows[Int32.Parse(Dd[1])].Cells[Int32.Parse(Dd[0])].Value == null)
            {
                DGV.Rows[Int32.Parse(Dd[1])].Cells[Int32.Parse(Dd[0])].Style = style;
            }
            else
            {

                try
                {
                    DateTime Dt;
                    Dt = DateTime.Parse(DGV.Rows[Int32.Parse(Dd[1])].Cells[Int32.Parse(Dd[0])].Value.ToString());
                    DGV.Rows[Int32.Parse(Dd[1])].Cells[Int32.Parse(Dd[0])].Style = styleGood;
                }
                catch (Exception ex)
                {
                    try
                    {
                        DateTime Dt = new DateTime(1899, 12, 31).AddDays(Int64.Parse(DGV.Rows[Int32.Parse(Dd[1])].Cells[Int32.Parse(Dd[0])].Value.ToString()));
                        DGV.Rows[Int32.Parse(Dd[1])].Cells[Int32.Parse(Dd[0])].Value = Dt.ToString("MM/dd/yyyy");
                        DGV.Rows[Int32.Parse(Dd[1])].Cells[Int32.Parse(Dd[0])].Style = styleGood;
                    }
                    catch (Exception Ex01)
                    {
                        DGV.Rows[Int32.Parse(Dd[1])].Cells[Int32.Parse(Dd[0])].Style = style;
                        error = 1;
                    }

                }
                if (error == 0 && DateTime.Parse(DGV.Rows[Int32.Parse(Dd[1])].Cells[Int32.Parse(Dd[0])].Value.ToString()) < DateTime.Now)
                {
                    DGV.Rows[Int32.Parse(Dd[1])].Cells[Int32.Parse(Dd[0])].Style = style;
                    error = 1; 
                    LblWarning.Text = "Please Check the fileds in Red. " + " The due date has passed.";
                }
            }

            //Client Name
            string[] CN = keys[3].Split(',');
            if (DGV.Rows[Int32.Parse(CN[1])].Cells[Int32.Parse(CN[0])].Value == null) { DGV.Rows[Int32.Parse(CN[1])].Cells[Int32.Parse(CN[0])].Style = style; error = 1;} else DGV.Rows[Int32.Parse(CN[1])].Cells[Int32.Parse(CN[0])].Style = styleGood;

            //Detailsss
            //First Record Row
            int FR = Int32.Parse(keys[4]);

            //NDC Col
            int ND = Int32.Parse(keys[5]);

            //Description
            int Ds = Int32.Parse(keys[6]);

            //Quantity
            int Qt = Int32.Parse(keys[7]);

            //Suggested Price
            int Su = Int32.Parse(keys[8]);

            //CSP Price
            int CS = Int32.Parse(keys[9]);

            //Total Price
            int To = Int32.Parse(keys[10]);

            //Manufacturer
            int Ma = Int32.Parse(keys[11]);

            //Country
            int Co = Int32.Parse(keys[12]);


            int n = 0;
            if (DGV.Rows[FR].Cells[Qt].Value == null)
            {
                try
                {
                    n = 1;
                    DGV.Rows[FR].Cells[ND].Style = style;
                    DGV.Rows[FR].Cells[Ds].Style = style;
                    DGV.Rows[FR].Cells[Qt].Style = style;
                    DGV.Rows[FR].Cells[Su].Style = style;
                    DGV.Rows[FR].Cells[CS].Style = style;
                    DGV.Rows[FR].Cells[To].Style = style;
                    DGV.Rows[FR].Cells[Ma].Style = style;
                    DGV.Rows[FR].Cells[Co].Style = style;
                    error = 1;
                }
                catch (Exception ex)
                { }
            }
            while (n == 0)
            {
                //NDC
                DGV.Rows[FR].Cells[ND].Style = styleGood;
                //Description
                DGV.Rows[FR].Cells[Ds].Style = styleGood;
                //Qty
                DGV.Rows[FR].Cells[Qt].Style = styleGood;
                //Suggested
                DGV.Rows[FR].Cells[Su].Style = styleGood;
                //CSP Price
                DGV.Rows[FR].Cells[CS].Style = styleGood;
                //Total Price
                DGV.Rows[FR].Cells[To].Style = styleGood;
                //Manufacurer
                DGV.Rows[FR].Cells[Ma].Style = styleGood;
                //Country
                DGV.Rows[FR].Cells[Co].Style = styleGood;
                FR = FR + 1;

                //Exit control
                try
                {
                    if (DGV.Rows[FR].Cells[Qt].Value == null) n = 1;
                }
                catch { n = 1; }
            }
            LblTemplateApplied.Text = template;
            if (error ==0) BtnSaveSN.Enabled = true; else
            {
                LblWarning.Visible = true;
            }

        }//End BtnCheck

    }//End Class
}//End NameSpace
