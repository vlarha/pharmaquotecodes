﻿namespace PharmaQuote.Controls
{
    partial class Quotes
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LblQuotes = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtQuotes1 = new System.Windows.Forms.TextBox();
            this.TxtQuotes = new System.Windows.Forms.TextBox();
            this.BtnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.DatePostedTo = new System.Windows.Forms.DateTimePicker();
            this.DatePostedFrom = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Cmb = new System.Windows.Forms.ComboBox();
            this.DateDueTo = new System.Windows.Forms.DateTimePicker();
            this.DateDueFrom = new System.Windows.Forms.DateTimePicker();
            this.TxtRFQ = new System.Windows.Forms.TextBox();
            this.labelRFQ = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.LblCount = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.DGV = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.RdoVendor = new System.Windows.Forms.RadioButton();
            this.RdoByItem = new System.Windows.Forms.RadioButton();
            this.rfQs011 = new PharmaQuote.Controls.RFQs01();
            this.quoteDetail01 = new PharmaQuote.Controls.QuoteDetail01();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(9, 206);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(400, 798);
            this.flowLayoutPanel1.TabIndex = 0;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.LblQuotes);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.TxtQuotes1);
            this.panel1.Controls.Add(this.TxtQuotes);
            this.panel1.Controls.Add(this.BtnSearch);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.DatePostedTo);
            this.panel1.Controls.Add(this.DatePostedFrom);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.Cmb);
            this.panel1.Controls.Add(this.DateDueTo);
            this.panel1.Controls.Add(this.DateDueFrom);
            this.panel1.Controls.Add(this.TxtRFQ);
            this.panel1.Controls.Add(this.labelRFQ);
            this.panel1.Location = new System.Drawing.Point(9, 22);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 150);
            this.panel1.TabIndex = 2;
            // 
            // LblQuotes
            // 
            this.LblQuotes.AutoSize = true;
            this.LblQuotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblQuotes.Location = new System.Drawing.Point(328, 70);
            this.LblQuotes.Name = "LblQuotes";
            this.LblQuotes.Size = new System.Drawing.Size(22, 13);
            this.LblQuotes.TabIndex = 17;
            this.LblQuotes.Text = "<->";
            this.LblQuotes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LblQuotes.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(288, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Quotes Condition";
            // 
            // TxtQuotes1
            // 
            this.TxtQuotes1.Location = new System.Drawing.Point(352, 66);
            this.TxtQuotes1.Name = "TxtQuotes1";
            this.TxtQuotes1.Size = new System.Drawing.Size(40, 20);
            this.TxtQuotes1.TabIndex = 15;
            this.TxtQuotes1.Text = "10";
            this.TxtQuotes1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtQuotes1.Visible = false;
            this.TxtQuotes1.TextChanged += new System.EventHandler(this.ValueChanged);
            this.TxtQuotes1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtQuotes_KeyPress);
            // 
            // TxtQuotes
            // 
            this.TxtQuotes.Location = new System.Drawing.Point(288, 66);
            this.TxtQuotes.Name = "TxtQuotes";
            this.TxtQuotes.Size = new System.Drawing.Size(40, 20);
            this.TxtQuotes.TabIndex = 14;
            this.TxtQuotes.Text = "0";
            this.TxtQuotes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.TxtQuotes.TextChanged += new System.EventHandler(this.ValueChanged);
            this.TxtQuotes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtQuotes_KeyPress);
            // 
            // BtnSearch
            // 
            this.BtnSearch.Location = new System.Drawing.Point(288, 114);
            this.BtnSearch.Name = "BtnSearch";
            this.BtnSearch.Size = new System.Drawing.Size(104, 23);
            this.BtnSearch.TabIndex = 13;
            this.BtnSearch.Text = "Search";
            this.BtnSearch.UseVisualStyleBackColor = true;
            this.BtnSearch.Click += new System.EventHandler(this.BtnSearch_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(149, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Posted To";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(149, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Posted From";
            // 
            // DatePostedTo
            // 
            this.DatePostedTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DatePostedTo.Location = new System.Drawing.Point(152, 115);
            this.DatePostedTo.Name = "DatePostedTo";
            this.DatePostedTo.Size = new System.Drawing.Size(119, 20);
            this.DatePostedTo.TabIndex = 10;
            this.DatePostedTo.ValueChanged += new System.EventHandler(this.ValueChanged);
            // 
            // DatePostedFrom
            // 
            this.DatePostedFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DatePostedFrom.Location = new System.Drawing.Point(152, 66);
            this.DatePostedFrom.Name = "DatePostedFrom";
            this.DatePostedFrom.Size = new System.Drawing.Size(119, 20);
            this.DatePostedFrom.TabIndex = 9;
            this.DatePostedFrom.ValueChanged += new System.EventHandler(this.ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Due To";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Due From";
            // 
            // Cmb
            // 
            this.Cmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb.FormattingEnabled = true;
            this.Cmb.Items.AddRange(new object[] {
            "=",
            ">",
            ">=",
            "<",
            "<=",
            "Between"});
            this.Cmb.Location = new System.Drawing.Point(288, 22);
            this.Cmb.Name = "Cmb";
            this.Cmb.Size = new System.Drawing.Size(104, 21);
            this.Cmb.TabIndex = 6;
            this.Cmb.SelectedIndexChanged += new System.EventHandler(this.Cmb_SelectedIndexChanged);
            this.Cmb.SelectedValueChanged += new System.EventHandler(this.ValueChanged);
            // 
            // DateDueTo
            // 
            this.DateDueTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateDueTo.Location = new System.Drawing.Point(12, 115);
            this.DateDueTo.Name = "DateDueTo";
            this.DateDueTo.Size = new System.Drawing.Size(119, 20);
            this.DateDueTo.TabIndex = 3;
            this.DateDueTo.ValueChanged += new System.EventHandler(this.ValueChanged);
            // 
            // DateDueFrom
            // 
            this.DateDueFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DateDueFrom.Location = new System.Drawing.Point(12, 67);
            this.DateDueFrom.Name = "DateDueFrom";
            this.DateDueFrom.Size = new System.Drawing.Size(119, 20);
            this.DateDueFrom.TabIndex = 2;
            this.DateDueFrom.ValueChanged += new System.EventHandler(this.ValueChanged);
            // 
            // TxtRFQ
            // 
            this.TxtRFQ.Location = new System.Drawing.Point(12, 22);
            this.TxtRFQ.Name = "TxtRFQ";
            this.TxtRFQ.Size = new System.Drawing.Size(259, 20);
            this.TxtRFQ.TabIndex = 1;
            this.TxtRFQ.TextChanged += new System.EventHandler(this.ValueChanged);
            // 
            // labelRFQ
            // 
            this.labelRFQ.AutoSize = true;
            this.labelRFQ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRFQ.Location = new System.Drawing.Point(9, 6);
            this.labelRFQ.Name = "labelRFQ";
            this.labelRFQ.Size = new System.Drawing.Size(32, 13);
            this.labelRFQ.TabIndex = 0;
            this.labelRFQ.Text = "RFQ";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Window;
            this.panel2.Controls.Add(this.LblCount);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.quoteDetail01);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(418, 1007);
            this.panel2.TabIndex = 3;
            // 
            // LblCount
            // 
            this.LblCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCount.Location = new System.Drawing.Point(292, 3);
            this.LblCount.Name = "LblCount";
            this.LblCount.Size = new System.Drawing.Size(117, 17);
            this.LblCount.TabIndex = 4;
            this.LblCount.Text = "0000";
            this.LblCount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 17);
            this.label6.TabIndex = 3;
            this.label6.Text = "Search Criteria";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.splitContainer1.Location = new System.Drawing.Point(424, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.rfQs011);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.DGV);
            this.splitContainer1.Panel2.Controls.Add(this.panel3);
            this.splitContainer1.Size = new System.Drawing.Size(846, 1007);
            this.splitContainer1.SplitterDistance = 218;
            this.splitContainer1.TabIndex = 5;
            // 
            // DGV
            // 
            this.DGV.AllowUserToAddRows = false;
            this.DGV.AllowUserToDeleteRows = false;
            this.DGV.BackgroundColor = System.Drawing.SystemColors.Window;
            this.DGV.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.DGV.Location = new System.Drawing.Point(0, 25);
            this.DGV.Name = "DGV";
            this.DGV.Size = new System.Drawing.Size(846, 760);
            this.DGV.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Window;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.RdoVendor);
            this.panel3.Controls.Add(this.RdoByItem);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(846, 25);
            this.panel3.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 4);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 17);
            this.label7.TabIndex = 4;
            this.label7.Text = "Quotes";
            // 
            // RdoVendor
            // 
            this.RdoVendor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RdoVendor.AutoSize = true;
            this.RdoVendor.Location = new System.Drawing.Point(724, 3);
            this.RdoVendor.Name = "RdoVendor";
            this.RdoVendor.Size = new System.Drawing.Size(106, 17);
            this.RdoVendor.TabIndex = 1;
            this.RdoVendor.Text = "Group By Vendor";
            this.RdoVendor.UseVisualStyleBackColor = true;
            // 
            // RdoByItem
            // 
            this.RdoByItem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RdoByItem.AutoSize = true;
            this.RdoByItem.Checked = true;
            this.RdoByItem.Location = new System.Drawing.Point(618, 3);
            this.RdoByItem.Name = "RdoByItem";
            this.RdoByItem.Size = new System.Drawing.Size(92, 17);
            this.RdoByItem.TabIndex = 0;
            this.RdoByItem.TabStop = true;
            this.RdoByItem.Text = "Group By Item";
            this.RdoByItem.UseVisualStyleBackColor = true;
            this.RdoByItem.CheckedChanged += new System.EventHandler(this.RdoBy_CheckedChanged);
            // 
            // rfQs011
            // 
            this.rfQs011.BackColor = System.Drawing.SystemColors.Window;
            this.rfQs011.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rfQs011.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rfQs011.Location = new System.Drawing.Point(0, 0);
            this.rfQs011.Margin = new System.Windows.Forms.Padding(0);
            this.rfQs011.Name = "rfQs011";
            this.rfQs011.Size = new System.Drawing.Size(846, 218);
            this.rfQs011.TabIndex = 5;
            // 
            // quoteDetail01
            // 
            this.quoteDetail01.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.quoteDetail01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.quoteDetail01.Location = new System.Drawing.Point(9, 171);
            this.quoteDetail01.Margin = new System.Windows.Forms.Padding(0);
            this.quoteDetail01.Name = "quoteDetail01";
            this.quoteDetail01.Size = new System.Drawing.Size(400, 36);
            this.quoteDetail01.TabIndex = 1;
            // 
            // Quotes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.panel2);
            this.Name = "Quotes";
            this.Size = new System.Drawing.Size(1280, 1007);
            this.Load += new System.EventHandler(this.Quotes_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGV)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private QuoteDetail01 quoteDetail01;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox Cmb;
        private System.Windows.Forms.DateTimePicker DateDueTo;
        private System.Windows.Forms.DateTimePicker DateDueFrom;
        private System.Windows.Forms.TextBox TxtRFQ;
        private System.Windows.Forms.Label labelRFQ;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker DatePostedTo;
        private System.Windows.Forms.DateTimePicker DatePostedFrom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button BtnSearch;
        private System.Windows.Forms.TextBox TxtQuotes1;
        private System.Windows.Forms.TextBox TxtQuotes;
        private System.Windows.Forms.Label LblQuotes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LblCount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private RFQs01 rfQs011;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton RdoVendor;
        private System.Windows.Forms.RadioButton RdoByItem;
        private System.Windows.Forms.DataGridView DGV;
        private System.Windows.Forms.Label label7;
    }
}
