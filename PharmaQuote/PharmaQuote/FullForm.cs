﻿using System;
using System.Drawing;
using System.Windows.Forms;
using PharmaQuote.Controls;

namespace PharmaQuote
{    
    public partial class FullForm : Form
    {
        string SelectionMenu = "Load";
        public FullForm()
        {
            InitializeComponent();
            LoadFile();
        }

        private void Lbl_MouseEnter(object sender, EventArgs e)
        {
            if (sender.GetType().ToString() == "System.Windows.Forms.Panel")
            {
                Panel Pn = (Panel)sender;
                Pn.BackColor = Color.DarkGray;
                foreach (Label l in Pn.Controls) l.ForeColor = Color.White;
            }
            if (sender.GetType().ToString() == "System.Windows.Forms.Label")
            {
                Label Lb = (Label)sender;
                Lb.ForeColor = Color.White;
                Panel Pn = Lb.Parent as Panel;
                Pn.BackColor = Color.DarkGray;
            }
        }

        private void Lbl_MouseLeave(object sender, EventArgs e)
        {
            if (sender.GetType().ToString() == "System.Windows.Forms.Panel")
            {
                Panel Pn = (Panel)sender;
                Pn.BackColor = MenuContainer.BackColor;
                foreach (Label l in Pn.Controls)
                {
                    if (l.Tag.ToString() == SelectionMenu) l.ForeColor = Color.White; else l.ForeColor = Color.Gray;
                }
            }
            if (sender.GetType().ToString() == "System.Windows.Forms.Label")
            {
                Label Lb = (Label)sender;
                if (Lb.Tag.ToString() == SelectionMenu) Lb.ForeColor = Color.White; else Lb.ForeColor = Color.Gray;
                Panel Pn = Lb.Parent as Panel;
                Pn.BackColor = MenuContainer.BackColor;
            }
        }

        private void Lbl_Click(object sender, EventArgs e)
        {
            foreach (object Ob in MenuContainer.Controls)
            {
                if (Ob.GetType().ToString() == "System.Windows.Forms.Panel")
                {
                    try
                    {
                        Panel Pn = (Panel)Ob;
                        if (SelectionMenu == Pn.Tag.ToString())
                        {
                            Pn.BackColor = MenuContainer.BackColor;
                            foreach (Label l in Pn.Controls)
                            {
                                l.ForeColor = Color.Gray;
                            }
                        }
                    }
                    catch { }
                }
            }//End foreach

            Boolean ChangeMenu = true;
            if (sender.GetType().ToString() == "System.Windows.Forms.Panel")
            {
                Panel Pn = (Panel)sender;
                if (Pn.Tag.ToString() == SelectionMenu) ChangeMenu = false;
                SelectionMenu = Pn.Tag.ToString();
            }
            else
            {
                Label Lb = (Label)sender;
                if (Lb.Tag.ToString() == SelectionMenu) ChangeMenu = false;
                SelectionMenu = Lb.Tag.ToString();
            }

            //If menu Change
            if (ChangeMenu)
            {
                switch (SelectionMenu)
                {
                    case "Load":
                        LoadFile();
                        break;
                    case "Quotes":
                        Quotes();
                        break;
                    case "Vendors":
                        Vendors();
                        break;
                    default:
                        
                        break;
                }
            }//End change menu
        }//End LblClick

        private void CleanContent()
        {
            Contenedor.Controls.Clear();
        }


        private void Quotes()
        {
            CleanContent();
            LblTittle.Text = "View Quotes";
            Quotes Lf = new Quotes();
            Lf.Padding = new Padding(5, 5, 5, 5);

            Contenedor.Controls.Add(Lf);
            Lf.Anchor = (AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Left);
            Lf.Dock = DockStyle.Fill;
        }
        private void Vendors()
        {
            CleanContent();
            LblTittle.Text = "Vendors";
            Vendors Lf = new Vendors();
            Lf.Padding = new Padding(5, 5, 5, 5);

            Contenedor.Controls.Add(Lf);
            Lf.Anchor = (AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Left);
            Lf.Dock = DockStyle.Fill;
        }
        private void LoadFile()
        {
            CleanContent();
            LblTittle.Text = "Load File";
            LoadFile Lf = new LoadFile();
            Lf.Padding = new Padding(5, 5, 5, 5);
            
            Contenedor.Controls.Add(Lf);
            Lf.Anchor= (AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Left);
            Lf.Dock = DockStyle.Fill;
        }

    }
}
